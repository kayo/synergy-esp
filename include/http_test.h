typedef union {
  struct {
    uint32_t chunk;
    uint32_t count;
  };
  size_t length;
} test_state_t;
