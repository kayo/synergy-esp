#include "sdk/sdk_config.h"

#include "marten.h"
#include "elastr.h"
#include "json_parser.h"
#include "param.h"
#include "lwip/tcp.h"

#ifdef HTTPD_SECURE
#include "ssl/lwipr_compat.h"
#endif

#ifndef HTTPD_BACKLOG
#define HTTPD_BACKLOG 32
#endif

#ifndef HTTPD_CONNPOOL
#define HTTPD_CONNPOOL 32
#endif

#ifndef HTTPD_BUFFER
#define HTTPD_BUFFER 32
#endif

#ifndef HTTPD_USE_YAML
#define HTTPD_USE_YAML 0
#endif

#define blob_def(name)       \
  extern char name[];        \
  extern uint32_t name##_len
#define blob_ptr(name) (name)
#define blob_len(name) (name##_len)

typedef struct {
  struct tcp_pcb *pcb;
  const marten_pool_t *pool;
} http_server_t;

#include HTTPD_MODULES_H

typedef struct {
  marten_conn_state_t state;
  marten_request_parser_t request;
  union {
    marten_query_parser_t query;
    marten_head_parser_t header;
  };
  marten_route_t route;
  
  marten_hash_t hash, key_hash;
  
  struct tcp_pcb *pcb;
  marten_response_writer_t response;
  struct {
    const char *ptr;
    uint32_t len;
  } send;

#ifdef HTTPD_SECURE
  SSL *ssl;
  SSLCTX* ssl_ctx;
#endif

  union {
    HTTPD_STATES
  };

  char buffer[HTTPD_BUFFER];
} http_conn_t;

extern http_server_t http_server;
#define server (&http_server)

extern const marten_router_t http_router;

#define marten_estr_write(writer, estr) marten_raw_write(writer, estr_str(estr), estr_len(estr))

#define DEBUG_GROUP httpd,HTTPD
#include "debug.h"

#ifdef HTTPD_DEBUG_IO
#define rawlog(fmt, ptr, len, ...) { \
    char str[len + 1];               \
    memcpy(str, ptr, len);           \
    str[len] = '\0';                 \
    debug(fmt " [[%s]]",             \
          ##__VA_ARGS__, str);       \
  }
#else
#define rawlog(...)
#endif

#ifdef HTTPD_ASSERT
#define assert(cond) if (!(cond)) { log(assert, "Assertion failed: ", #cond); }
#else
#define assert(...)
#endif

const char *ent_name(marten_flags_t flags);
const char *ent_part(marten_flags_t flags);

#define flags_fmt "[%s%s%s%s]"

#define show_flags(flags)                        \
  (flags & marten_init ? "init " : ""),          \
    (flags & marten_done ? "done " : ""),        \
    ent_name(flags), ent_part(flags)
