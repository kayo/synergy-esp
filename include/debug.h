#ifndef __DEBUG_H__
#define __DEBUG_H__

#define _DEBUG_GROUP_DEF_(key, def) def##_DEBUG
#define _DEBUG_GROUP_DEF(...) _DEBUG_GROUP_DEF_(__VA_ARGS__)
#define DEBUG_GROUP_DEF _DEBUG_GROUP_DEF(DEBUG_GROUP)

#define _DEBUG_GROUP_KEY_(key, def) #key
#define _DEBUG_GROUP_KEY(...) _DEBUG_GROUP_KEY_(__VA_ARGS__)
#define DEBUG_GROUP_KEY _DEBUG_GROUP_KEY(DEBUG_GROUP)

#define log(pri, fmt, ...) os_printf("%s" fmt "\n", DEBUG_GROUP_KEY "[" #pri "]: ", ##__VA_ARGS__)

#define _DEBUG_LEVEL_debug 0
#define _DEBUG_LEVEL_info 1
#define _DEBUG_LEVEL_error 2
#define _DEBUG_LEVEL_none 3

#define _DEBUG_LEVEL_(level) _DEBUG_LEVEL_##level
#define _DEBUG_LEVEL(...) _DEBUG_LEVEL_(__VA_ARGS__)
#define _DEBUG_IF_(level, target) (_DEBUG_LEVEL(level) >= _DEBUG_LEVEL(target))
#define _DEBUG_IF(target) _DEBUG_IF_(DEBUG_GROUP_DEF, target)

#if _DEBUG_IF(info)
#define info(fmt, ...) log(info, fmt, ##__VA_ARGS__)
#else
#define info(fmt, ...)
#endif

#if _DEBUG_IF(debug)
#define debug(fmt, ...) log(debug, fmt, ##__VA_ARGS__)
#else
#define debug(fmt, ...)
#endif

#if _DEBUG_IF(error)
#define error(fmt, ...) log(error, fmt, ##__VA_ARGS__)
#else
#define error(fmt, ...)
#endif

#endif /* __DEBUG_H__ */
