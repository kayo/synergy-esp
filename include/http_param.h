typedef union {
  struct {
    size_t param_coll;
    size_t param;
    estr_t result;
  } info;
  struct {
    const param_desc_t *param;
    estr_t field;
    estr_t result;
    json_tokenizer tokenizer;
    enum {
      param_prepare, /* prepare request */
      param_get_key, /* get request, receiving key */
      param_set_key, /* set request, receiving key */
      param_set_val, /* set request, receiving value */
      param_complete, /* complete request */
    } state;
  } data;
} param_state_t;
