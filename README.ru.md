Прошивка Synergy ESP
====================

Революционная прошивка для ESP8266.

Устройство прошивки
-------------------

В отличии от большинства существующих прошивок и фреймворков, **synergy** использует меньше ресурсов ввиду специфического дизайна, так что она позволяет реализовать более сложные решения для конечного пользователя.

### Ключевые особенности

* Настраиваемость
* Надёжность
* Оптимальность

### Строительные блоки

Эта прошивка построена на стеке различных программных компонентов.
Каждый компонент разработан специально для встраиваемых устройств с ограниченными реурсами RAM и CPU.

#### ESP SDK

> Альтернативный улучшенный ESP SDK

Для работы с платформой *ESP8266* используется альтернативная библиотека ESP SDK [katyo:ESP_SDK_Library](//github.com/katyo/ESP_SDK_Library).
Она имеет собственную реализацию правил сборки, которые позволили использовать её как подмодуль в этом проекте.
Также она реализует альтернативный способ связывания, с которым нам не требуется модифицировать код дополнительных библиотек для использования в этом проекте.

#### Сервер HTTP

> Конструктор для создания ультра-лёгких HTTP серверов и клиентов

При реализации встроенного HTTP сервера, был использован конструктор [kayo:libmarten](//bitbucket.org/kayo/libmarten).
В отличии от традиционных реализаций web серверов, *libmarten* реализует событийный разбор запросов и отправку ответов с помощью сопрограмм.
Нам требуется всего-лишь ~100 байт на каждое соединение (на уровне приложения), поскольку он оптимизирован для устройств с малым объёмом ОЗУ.
Это делает потенциально возможным обрабатывать десятки запросов одновременно, включая источники отправляемых сервером сообщений (SSE) и web соекты.

Библиотека позволила создать встроенный HTTP-сервер не вносящий накладных расходов на уровне прикладного протокола.
Поскольку библиотека предоставляет не законченный сервер, а лишь набор инструментов для разбора запросов, то у приложения есть возможность выбора необходимых инструментов в каждом конкретном случае.
В этой прошивке разбор запросов осуществляется в многоуровневом режиме.
Сперва данные попадают в разборщик протокола, который выделяет строку запроса, метод, протокол, блок заголовков, тело.
Затем полученная строка запроса подаётся на маршрутизатор, который выбирает обработчик, подходящий для дальнейшей обработки запроса.

Каждый обработчик сам решает, что производить с данными запроса. Например, обработчик `GET /` может сразу завершить обработку запроса, и начать отдавать содержимое собранённого во флеш файла `index.html.gz`.
В то время, как обработчик `GET /param`, при использовании авторизации для доступа к настройкам должен произвести разбор и проверку соответствующих заголовков для разрешения доступа.
Запрос может состоять из любого числа частей, поступающих последовательно в разное время, которые могут разрываться в любом месте.
Разборщик гарантирует корректность разбора в любых случаях.
Формирование ответа по HTTP организовано с использование C-сопрограмм.
Это позволило внедрить асинхронную обработку ответов, что упрощает организацию таких протоколов как Web-Socket и Server-Sent Events.

#### Конфигурация устройства

> Основанное на параметрах управление устройством

Управление функциями устройства реализовано с использованием [kayo:libparam](//bitbucket.org/uprojects/libparam).
Параметры представляют собой переменные, которые оказывают влияние на внутренее поведение прошивки и доступны извне.
Библиотека *libparam* позволяет изменять конфигурацию устройства через обобщённый интерфейс.
Этот интерфейс простой и самодостаточный и обеспечивает следующие возможности:

* Получение информации о параметрах (интроспекция)
* Получение значений читаемых параметров
* Изменение значений изменяемых параметров

Параметры главным образом классифицируются по типам значений.
Кроме того *libparam* позволяет контролировать корректность значений с помощью ограничений.
В дополнение библиотека берёт на себя сохранение и восстановление значений параметров используя постоянное хранилище (флеш память).

Родной способ конфигурации Espressif SDK теперь не рекомендуется.
Это означает, что вам **требуется** очистить область конфигурации в флеш памяти перед установкой **synergy**.

#### Разбор JSON

> Поточный ориентированный на функции обратного вызова разборщик JSON

Использование JSON для обмена данными является простым и распространённым способом взаимодействия посредством HTTP.
**synergy** разбирает JSON данные с использованием [kayo:json_parser](https://bitbucket.org/kayo/json_parser).
Эта библиотека реализует тот же способ работы как *libmarten*.

### Конструирование строк

> Расширяемые строки с удобными функциями отображения и несколькими бэкендами

Работа со строками традиционно слабая сторона программирования на C.
Здесь применяется реализация строк [kayo:elastr](//bitbucket.org/kayo/elastr).
Она позволяет получать строки путём добавления значений различных типов к концу.
Строки могут выделяться с использованием статических буферов или кучи.
Когда строка выделена в куче, она может расти автоматически, когда требуется.

Интерфейс пользователя
----------------------

### Web клиент

Web клиент реализует базовый интерфейс пользователя для настройки устройства.
Основные концепции изложены далее:

#### Одностраничное приложение

> Современные тенденции web-приложений это т.н. *"толстые"* клиенты

Начальный html, стили и сценарии организованы в виде единого HTML файла, который сжат и сохранён в флеш памяти устройства.
В соответствии с принципами разработки толстых клиентов, больше нет необходимости делить приложения на множество частей.
Для встраиваемых приложений это даёт свои преимущества в виде уменьшения используемого клиентом места в памяти, а также ускорения загрузки.
Также снимается необходимость в организации встроенных файловых систем для хранения множества статичных файлов.

#### Дружественность к носимым устройствам

> Web-интерфейс оптимизирован под браузеры носимых устройств

Удобный пользовательский интерфейс должен работать на любом современном мобильном браузере.
В клиенте используется легковесный модульный CSS фреймворк [picnic](https://github.com/picnicss/picnic).
Анимации организованы средствами таблиц стилей для снижения нагрузки на клиент.

#### Явное состояние

> Явное состояние вместо неявного
        
Явное состояние позволяет упростить разработку сложных интерфейсов пользователя путём устранения неопределённостей в поведении элементов интерфейса, неизбежно имеющих место быть в случае неявного состояния.
Состояние находится на стороне контроллера (в терминах модели MVC) или являет собой модель представления (в терминах методологии MVVM).
Представление синхронизируется с явным состоянием, когда последнее меняется.

#### Виртуальная DOM

> Продвинутая технология Virtual DOM

Здесь применяется продвинутая технология синхронизации представления с состоянием, носящая название Virtual DOM.
Суть её состоит в том, что в приложении существует виртуальное дерево тегов HTML документа, которое получается путём отображения состояния.
При изменении состояния происходит повторное отображение, которое затем сравнивается с предыдущим отображением, а изменения применяются к реальному DOM.
Поскольку JavaScript в современных браузерах выполняется много быстрее вызовов DOM API, описанный подход оптимизирует необходимые модификации реального документа при обновлениях.

В клиенте применена библиотека [Snabbdom](https://github.com/paldepind/snabbdom), являющаяся одной из самых быстрых и легковесных реализаций технологии Virtual DOM.

#### Кофе-скрипт

> Мощный, выразительный и чистый язык сценариев

Это просто крутая штука, которая используется, чтобы сделать код клиента похожим на искусство, а не на ад.

### REST API

Web-склиент использует этот API для взаимодействия с устройством, но и другие приложения тоже могут использовать его с то же целью.

#### Получение описаний параметров

На запрос `GET /param` возращается JSON-массив с объектами, описывающими параметры.
Каждое описание может иметь следующие поля:

* `type`: "тип параметра"
* `size`: <размер параметра>
* `flag`: [набор флагов]
* `def`: <значение по-умолчанию>
* `min`: <минимальное значение>
* `max`: <мачимальное значение>
* `stp`: <шаг для значений> (считается от минимума или максимума)
* `name`: "название_параметра"
* `desc`: "описание параметра"
* `enum`: {возможные значения перечисления}

Параметры могут иметь следующие типы:

* `none` - отсутствие типа (параметры без значений не могут иметь тип)
* `uint` - беззнаковое целое число (реальный тип зависит от размера в байтах)
* `sint` - знаковое целое число (реальный тип зависит от размера в байтах)
* `real` - число с плавающей точкой (реальный тип зависит от размера в байтах)
* `enum` - перечисление (возможные значения перечисления возвращаются в поле `enum`)
* `cstr` - строки символов, завершающиеся нулём ('\0')
* `hbin` - бинарные данные, представленные в шестнадцатеричном виде (hex sequences)
* `ipv4` - адрес IPv4 в каноническом представлении (192.168.10.100)
* `mac` - MAC адрес в каноническом представлении (AA:BB:CC:DD:EE:FF)

Возможные значения перечислений представлены в поле `enum` в виде объекта вида:

* "значение_а": "Описание значения А"
* "значение_б": "Описание значения Б"
* ...

Флаги представлены в поле `flag` в виде массива строк.
Следующие флаги могут быть возвращены:

* `persist` - значение параметра хранится в постоянном хранилище
* `virtual` - параметр виртуальный (для доступа к значению используются getter/setter)
* `readable` - значение параметра может быть получено
* `writable` - значение параметра может быть изменено

Для объединения параметров в группы могут быть использованы параметры без типа (`none`).

Также с помощью параметров удобно реализовать константы, например, номер версии.
Такие параметры имеют значение по-умолчанию, но их значение нельзя получать и изменять.

#### Получение значений параметров

Запрос `PUT /param` с JSON-массивом имён параметров в теле запроса инициирует получение значений.
Возвращается JSON-массив значений.

#### Изменение значений параметров

Запрос `PUT /param` с JSON-объектом имён параметров в качестве ключей и новых значений в качестве значений в теле запроса инициирует изменение значений.
Возвращается пустое тело ответа.

#### Сканирование беспроводной сети

Запрос `GET /scan` начинает сканирование беспроводной сети.
В качестве ответа возвращается JSON-массив с объектами, описывающими точки доступа.
Эти объекты содержат следующие поля:

* `bssid`: "bssid сети" ("aa:bb:cc:dd:ee:ff")
* `ssid`: "идентификатор сети"
* `auth`: "тип требуемой авторизации" (open, wep, wpa, wpa2...)
* `channel`: <используемый канал>
* `fqoff`: <смещение частоты>
* `rssi`: <индикатор качества сигнала>
