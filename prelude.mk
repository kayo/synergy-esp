# Base path to build root
synergy.BASEPATH := $(subst $(dir $(abspath $(CURDIR)/xyz)),,$(dir $(abspath $(lastword $(MAKEFILE_LIST)))))
synergy.INCDIR := $(synergy.BASEPATH)include
synergy.SRCDIR := $(synergy.BASEPATH)src
synergy.WEBDIR := $(synergy.BASEPATH)html
synergy.LIBSDIR := $(synergy.BASEPATH)libs
synergy.SDKDIR := $(synergy.LIBSDIR)/espsdk

include $(synergy.SDKDIR)/prelude.mk

espsdk.no_esp_config := y
include $(synergy.SDKDIR)/rules.mk

include $(synergy.LIBSDIR)/marten/martenpp.mk
libmartenpp.INHERIT := host
martenpp.INHERIT := host
$(call use_toolchain,host)
host.INHERIT := stalin

include $(synergy.LIBSDIR)/marten/marten.mk
libmarten.INHERIT := firmware
libmarten.CDEFS += MARTEN_CSTR_ATTR=ICACHE_RODATA_ATTR

include $(synergy.LIBSDIR)/json_parser/json_parser.mk
libjson_parser.INHERIT := firmware

TARGET.LIBS += libelastr
libelastr.INHERIT := firmware
libelastr.CDEFS! += realloc=port_realloc malloc_usable_size=port_malloc_usable_size
libelastr.SRCS += $(wildcard $(synergy.LIBSDIR)/elastr/*.c)
libelastr.CDIRS += $(synergy.LIBSDIR)/elastr
#include $(synergy.LIBSDIR)/elastr/rules.mk

libparam.types ?= enum uint sint real cstr hbin mac ipv4
libparam.texts ?= unit info item hint
libparam.flags ?= persist virtual fix def min max stp 8bit 16bit 32bit
libparam.parts := sio json estr
libparam.store := esp8266-spi-flash
include $(synergy.LIBSDIR)/param/rules.mk
libparam.INHERIT := firmware libelastr libjson_parser
libparam.CDEFS += PARAM_ROM_ALIGN=4
libparam.CDEFS += PARAM_ROM_ATTR=ICACHE_RODATA_ATTR
