# Base module
synergy.MODULES += base
base.OPTION_DEFS := $(addprefix $(synergy.SRCDIR)/,\
  common.cf\
  radio.cf)
base.PARAM_DEFS := $(addprefix $(synergy.SRCDIR)/,\
  common.cf \
  radio.cf)
base.HTTPD_INCLUDES := \
  http_param.h \
  http_scan.h
base.HTTPD_STATES := \
  param_state_t:param \
  scan_state_t:scan
base.HTTPD_ROUTES := \
  GET:/:get_root_handler \
  GET:/param:get_param_handler \
  PUT:/param:put_param_handler \
  GET:/scan:get_scan_handler
base.HTMLC_MODULES := settings devmon radio
base.SRCS := $(addprefix $(synergy.SRCDIR)/,\
  common.c \
  radio.c \
  http_server.c \
  http_index.c \
  http_param.c \
  http_scan.c) \
  $(patsubst %,%.blob.S,$(INDEX_HTML_GZ))

# Info module
synergy.MODULES += info
info.HTMLC_MODULES := info

# Test module
synergy.MODULES += test
test.HTTPD_INCLUDES := \
  http_test.h
test.HTTPD_STATES := \
  test_state_t:test
test.HTTPD_ROUTES := \
  GET:/ping:get_ping_handler \
  GET:/test:get_test_handler \
  PUT:/test:put_test_handler
test.HTMLC_MODULES := test
test.SRCS := $(addprefix $(synergy.SRCDIR)/,\
  http_ping.c \
  http_test.c)

#synergy.MODULES += term
term.HTTPD_INCLUDES := \
  http_term.h
term.HTTPD_STATES := \
  term_state_t:term
term.HTTPD_ROUTES := \
  GET:/term/sse:get_term_handler \
  PUT:/term:put_term_handler
term.HTMLC_MODULES := term
term.SRCS := $(addprefix $(synergy.SRCDIR)/,\
  http_term.c)
