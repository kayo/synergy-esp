# <ent> <list> <yes> <no>
# if <ent> is last in <list> then return <yes> else return <no>
ENT_LAST = $(if $(filter-out $(1),$(lastword $(2))),$(3),$(4))

COL_WORD = $(word $(1),$(subst :, ,$(2)))

GEN_H_P = gen/synergy/$(1).sg.h

PARAM_MODULES_H := $(call GEN_H_P,param_modules)
$(synergy.SRCDIR)/http_param.c: $(PARAM_MODULES_H)
$(PARAM_MODULES_H): $(MAKEFILE_LIST)
	@echo GEN PARAM MODULES $(PARAM_COLLS)
	$(Q)mkdir -p $(dir $@)
	$(Q)echo '/* Automatically generated file' > $@
	$(Q)echo '   DO NOT TOUCH THIS */' >> $@
	$(Q)$(foreach mod,$(PARAM_COLLS),echo 'extern param_coll_t $(mod);' >> $@;)
	$(Q)echo >> $@
	$(Q)echo '#define PARAM_COLLS \\' >> $@
	$(Q)$(foreach mod,$(PARAM_COLLS),echo '  &$(mod)$(call ENT_LAST,$(mod),$(PARAM_COLLS),$(strip ,) \\)' >> $@;)

clean: clean.param_modules
clean.param_modules:
	@echo CLEAN PARAM MODULES $(PARAM_COLLS)
	$(Q)rm -f $(PARAM_MODULES_H)

HTTPD_MODULES_H := $(call GEN_H_P,httpd_modules)
$(synergy.SRCDIR)/http_server.c: $(HTTPD_MODULES_H)
$(HTTPD_MODULES_H): $(MAKEFILE_LIST)
	@echo GEN HTTPD MODULES $(HTTPD_INCLUDES)
	$(Q)mkdir -p $(dir $@)
	$(Q)echo '/* Automatically generated file' > $@
	$(Q)echo '   DO NOT TOUCH THIS */' >> $@
	$(Q)$(foreach mod,$(HTTPD_INCLUDES),echo '#include "$(mod)"' >> $@;)
	$(Q)echo >> $@
	$(Q)echo '#define HTTPD_STATES \\' >> $@
	$(Q)$(foreach mod,$(HTTPD_STATES),echo '  $(call COL_WORD,1,$(mod)) $(call COL_WORD,2,$(mod));$(call ENT_LAST,$(mod),$(HTTPD_STATES), \\)' >> $@;)
	$(Q)echo >> $@
	$(Q)echo '#define HTTPD_ROUTES \\' >> $@
	$(Q)$(foreach mod,$(HTTPD_ROUTES),echo '  ("$(call COL_WORD,1,$(mod))", "$(call COL_WORD,2,$(mod))", $(call COL_WORD,3,$(mod)))$(call ENT_LAST,$(mod),$(HTTPD_ROUTES),$(strip ,) \\)' >> $@;)
	$(Q)echo >> $@
	$(Q)$(foreach mod,$(HTTPD_ROUTES),echo 'marten_define_handler($(call COL_WORD,3,$(mod)));' >> $@;)

clean: clean.httpd_modules
clean.httpd_modules:
	@echo CLEAN HTTPD MODULES $(HTTPD_INCLUDES)
	$(Q)rm -f $(HTTPD_MODULES_H)

WEB_P = gen/web_client/$(1)
MODULES_COFFEE := $(call WEB_P,modules.coffee)
INDEX_HTML_GZ := $(call WEB_P,index.html.gz)
HTMLC_DEPS_MK := $(call WEB_P,depends.mk)

$(MODULES_COFFEE): $(MAKEFILE_LIST)
	@echo GEN HTML MODULES $(HTMLC_MODULES)
	$(Q)mkdir -p $(dir $@)
	$(Q)echo '# Automatically generated file' > $@
	$(Q)echo '# DO NOT TOUCH THIS' >> $@
	$(Q)echo 'module.exports = [' >> $@
	$(Q)$(foreach mod,$(HTMLC_MODULES),echo '  require "$(mod)"' >> $@;)
	$(Q)echo ']' >> $@

clean: clean.htmlc_modules
clean.htmlc_modules:
	@echo CLEAN HTML MODULES $(HTMLC_MODULES)
	$(Q)rm -f $(MODULES_COFFEE)

HTMLC_DIRS += $(dir $(MODULES_COFFEE))
HTMLC_ENV += OUTPUT_DIR=$(realpath $(dir $(INDEX_HTML_GZ)))
HTMLC_ENV += MODULES_PATH=$(subst $(eval) ,:,$(foreach dir,$(strip $(HTMLC_DIRS)),$(realpath $(dir))))
HTMLC_ENV += DEPENDS_TARGET='$$(INDEX_HTML_GZ)'

$(INDEX_HTML_GZ): $(MODULES_COFFEE)
	@echo GEN HTML CLIENT
	$(Q)cd $(synergy.WEBDIR) && $(HTMLC_ENV) $(if $(wildcard $(synergy.WEBDIR)/node_modules),cake $(if $(findstring y,$(HTMLC_DEBUG)),-d) build,npm install)

-include $(HTMLC_DEPS_MK)

clean: clean.html_client
clean.html_client:
	@echo CLEAN HTML CLIENT
	$(Q)rm -rf web

OPTION_DEFS = $(foreach mod,$(synergy.modules),$($(mod).OPTION_DEFS))
PARAM_DEFS = $(foreach mod,$(synergy.modules),$(foreach def,$($(mod).PARAM_DEFS),$(notdir $(basename $(def)))_params:$(def):$(param.language)))
PARAM_COLLS = $(foreach mod,$(synergy.modules),$(patsubst %,%_params,$(notdir $(basename $($(mod).PARAM_DEFS)))))
HTTPD_INCLUDES = $(foreach mod,$(synergy.modules),$($(mod).HTTPD_INCLUDES))
HTTPD_STATES = $(foreach mod,$(synergy.modules),$($(mod).HTTPD_STATES))
HTTPD_ROUTES = $(foreach mod,$(synergy.modules),$($(mod).HTTPD_ROUTES))
HTMLC_MODULES = $(foreach mod,$(synergy.modules),$($(mod).HTMLC_MODULES))
HTMLC_DIRS += $(foreach mod,$(synergy.modules),$($(mod).HTMLC_DIRS))

TARGET.LIBS += libsynergy
libsynergy.INHERIT += marten-pp libelastr libjson_parser libsdk libparam libmarten

marten.use_pp ?= yes

TARGET.OPTS += libsynergy
libsynergy.OPTS = $(synergy.SRCDIR)/synergy.cf

libsynergy.OPTS += $(OPTION_DEFS)
libsynergy.PARS += $(PARAM_DEFS)
libsynergy.CDIRS += $(dir $(PARAM_MODULES_H)) $(dir $(HTTPD_MODULES_H))
libsynergy.CDEFS += PARAM_MODULES_H=\"$(notdir $(PARAM_MODULES_H))\"
libsynergy.CDEFS += HTTPD_MODULES_H=\"$(notdir $(HTTPD_MODULES_H))\"
libsynergy.CDIRS += include $(synergy.INCDIR)
libsynergy.CDIRS += $(foreach mod,$(synergy.modules),$($(mod).CDIRS))
libsynergy.SRCS += $(addprefix src/, \
  firmware.c)
libsynergy.SRCS += $(INDEX_HTML_GZ)
libsynergy.SRCS += $(foreach mod,$(synergy.modules),$($(mod).SRCS))

TARGET.IMGS += synergy
synergy.INHERIT = firmware
synergy.DEPLIBS& += libsdk libelastr libjson_parser libparam libmarten libsynergy

info.modules:
	@$(foreach v,OPTION_DEFS PARAM_DEFS PARAM_COLLS HTTPD_INCLUDES HTTPD_STATES HTTPD_ROUTES HTMLC_MODULES HTMLC_DIRS,echo '$(v): $($(v))';)
