Synergy Firmware ESP
====================

Revolutionary firmware for ESP8266.

Firmware design
---------------

Unlike most existing firmwares and frameworks, **synergy** uses less resources by design, and so it allows implement more complex end-user solutions.

### Key features

* Customizability
* Reliability
* Optimality

### Building blocks

This firmware built on stack of different software components.
Each component developed especially for embedded devices with restrictive RAM and CPU.

#### ESP SDK

> Alternative improved ESP SDK

To deal with *ESP8266* platform used alternative ESP SDK library [katyo:ESP_SDK_Library](//github.com/katyo/ESP_SDK_Library).
This SDK has own implementation of build rules which allows use it as submodule into this project.
Also it implements alternative linking way, with which we don't need to modify code of additional libraries to use in project.

#### HTTP Server

> Ultra light-weight HTTP server/client construction kit

To implement built-in HTTP server used HTTP Construction Kit [kayo:libmarten](//bitbucket.org/kayo/libmarten).
Unlike traditional web server implementations *libmarten* implements event-driven request parsing and coroutine-like response writing.
We needed only ~100 bytes per each client connection (on application layer), because it optimized for devices with small RAM size.
That makes potentially possible to serve tens requests simultaneously, including server-sent event sources and web sockets.

#### Device Config

> Parameter-based hardware control

Controlling device functions implemented using [kayo:libparam](//bitbucket.org/uprojects/libparam).
The *libparam* allows us change device configuration through generalized interface.
This interface is simple but self-sufficient and provides next capabilities:

* Getting descriptions of parameters (introspection)
* Getting values of readable parameters
* Changing values of writable parameters

Mainly the parameters classified by types of values.
Besides the *libparam* allows validate values using constraints.
In addition this library implements saving and loading values of parameters using persistent storage (flash memory).

Espressif's native configuring way is now deprecated.
This means that you **must** clean config area in flash before installing **synergy**.

#### JSON Parsing

> Streamed callback-oriented JSON parser

Using JSON to data exchange is simple and common way to interfacing over HTTP.
**Synergy** parses JSON data using [kayo:json_parser](https://bitbucket.org/kayo/json_parser).
This library implements same way of operating as *libmarten*.

#### String Constructing

> Growable strings with usable rendering features and multiple backends

Working with strings traditionally is weak side of C-programming.
Here is used string implementation [kayo:elastr](//bitbucket.org/kayo/elastr).
It allows render string by appending values of different types.
The strings can be allocated using static buffers or heap memory.
When string allocated in heap, it can grow automatically, when required.

User interface
--------------

### Web client

Web client implements basic user interface to configuration of device.
The main concepts are described below.

#### Single page application

> Modern trends in web apps is *"fat"* clients

The initial html, styles and scenariuos organized as single html file, which have been compressed and saved on device flash memory.

#### Mobile friendly

> Web-interface optimized for mobile browsers

Comfortable user interface must work on any modern mobile browser.

#### Explicit state

> Explicit state instead of implicit state

This development technique helps implement bug-free view-models (or controllers in MVC terms) with strict deterministic behavior.
The state placed at controller side (in MVC terms) or represents view-model (in MVVM terms).
The view is synchronized with explicit state, when state is changed.

#### Virtual DOM

> Advanced technology Virtual DOM

Here used advanced technology of synchronization views with states.
The idea is in using virtual tags tree of HTML document, which produced as result of rendering explicit state.
On each change of state it initiates rendering to get new virtual tree of HTML document, which compares with previous tree to find differences, which need to be applied to real DOM.
Because JavaScript in modern browsers executes much faster than DOM API calls, so described technique optimizes necessary modifications of real document on updates.

In client used [Snabbdom](https://github.com/paldepind/snabbdom) library, which is one of faster and lighter implementations of Virtual DOM.

#### CoffeeScript

> Powerful, expressive and clear scripting language

This is just cool thing, which used to make client code looks like an art not a hell.

### REST API

Web-client uses this API for interfacing with device but other applications also can use it in same purpose.

#### Getting parameter descriptions

The `GET /param` request get JSON-array with objects, which describes parameters.
Each description may have next fields:

* `type`: "parameter type"
* `size`: <parameter size>
* `flag`: [set of flags]
* `def`: <default value>
* `min`: <minimum value>
* `max`: <maximum value>
* `stp`: <value step> (counted from minimum or maximum)
* `name`: "parameter name"
* `desc`: "parameter description"
* `enum`: {possible values of enumeration}

Parameters can be of the next types:

* `none` - none type (parameter without value can't have type)
* `uint` - unsigned integer number (real type depends from size in bytes)
* `sint` - signed integer number (real type depends from size in bytes)
* `real` - floating point number (real type depends from size in bytes)
* `enum` - enumeration (possible values are returned in `enum` field)
* `cstr` - null-terminated ('\0') string of characters
* `hbin` - binary data, which represented as hexadecimal sequence
* `ipv4` - IPv4 address in canonical pepresentation (192.168.10.100)
* `mac` - MAC address in canonical representation (AA:BB:CC:DD:EE:FF)

Possible values of enumerations described in `enum` field as array in next form:

* "value_a": "Description of value A"
* "value_b": "Description of value B"
* ...

Flags represented in `flag` field as array of strings.
The next flags can be returned:

* `persist` - the value of parameter is saved in persistent storage
* `virtual` - the parameter is virtual (getter/setter are used for access to value)
* `readable` - the value of parameter can be got
* `writable` - the value of parameter can be changed

For combining parameters in groups you can use parameters without type (`none`).

Also you can implement constants, for example, version number.
Such parameters have default values, but its value can not be got or changed.

#### Getting parameter values

The `PUT /param` request with JSON-array of parameter names in body initiates getting of values.
JSON-array of values is returned in response body.

#### Changing parameter values

The `PUT /param` request with JSON-object of parameter names as keys and new values as values in body initiates changing ov values.
Empty body is returned as response.

#### Scanning wireless network

The `GET /scan` request initiates scanning of wireless network area.
As response returned JSON-array with objects, each of this describes access point.

These objects consists of next fields:

* `bssid`: "network bssid" ("aa:bb:cc:dd:ee:ff")
* `ssid`: "network identifier"
* `auth`: "required auth type" (open, wep, wpa, wpa2...)
* `channel`: <used channel>
* `fqoff`: <frequency offset>
* `rssi`: <signal strength indicator>
