{get, put} = require "req"
{Plotter} = require "plot"

now = -> do (new Date).getTime

@Ctrl = (Base)->
  class Test extends Base
    ping_plot_results: 100
    ping_view_results: 10
    ping_max_results: 100
    ping_timeout: 1000

    upload_plot_results: 100
    upload_view_results: 10
    upload_max_results: 100

    download_plot_results: 100
    download_view_results: 10
    download_max_results: 100
    
    constructor: ->
      super
      @ping_result = []
      @ping_enable = no
      @ping_errors = []
      @ping_plotter = new Plotter
        points: @ping_result
        getter: ({ping, pong})-> [ping, pong - ping]
        aspect: 2
        yrange: (min_y, max_y)-> [0, max_y]
        xrange: (min_x, max_x, n)=> [min_x, min_x + (max_x - min_x) * @ping_plot_results / n]
        lineWidth: 2
        lineStyle: "rgba(174,66,63,0.8)"
        fillStyle: "rgba(217,83,79,0.3)"

      @upload_result = []
      @upload_enable = no
      @upload_errors = []
      @upload_plotter = new Plotter
        points: @upload_result
        getter: ({time, speed})-> [time, speed]
        aspect: 2
        yrange: (min_y, max_y)-> [0, max_y]
        xrange: (min_x, max_x, n)=> [min_x, min_x + (max_x - min_x) * @upload_plot_results / n]
        lineWidth: 2
        lineStyle: "rgba(174,66,63,0.8)"
        fillStyle: "rgba(217,83,79,0.3)"
      
      @download_result = []
      @download_enable = no
      @download_errors = []
      @download_plotter = new Plotter
        points: @download_result
        getter: ({time, speed})-> [time, speed]
        aspect: 2
        yrange: (min_y, max_y)-> [0, max_y]
        xrange: (min_x, max_x, n)=> [min_x, min_x + (max_x - min_x) * @download_plot_results / n]
        lineWidth: 2
        lineStyle: "rgba(174,66,63,0.8)"
        fillStyle: "rgba(217,83,79,0.3)"

    #
    # Latency test
    #

    trigger_ping: =>
      if @ping_enable
        ping = do now
        get "/ping", (err, res)=>
          if err
            @ping_errors.push err
            @ping_enable = no
          else
            pong = do now
            
            @ping_result.unshift {ping, pong} if res is "pong"
            @ping_result[@ping_max_results...] = [] if @ping_result.length > @ping_max_results
            
            setTimeout @trigger_ping, @ping_timeout if @ping_enable
          
          do @$render
          do @ping_plotter.redraw

    clear_ping_result: =>
      @ping_result[...] = []
      do @$render
      do @ping_plotter.redraw

    clear_ping_error: (index)=>
      @ping_errors.splice index, 1
      do @$render
    
    set_ping_enable: (@ping_enable)=>
      do @trigger_ping
      do @$render

    $resize: ->
      super
      do @ping_plotter.redraw
      do @upload_plotter.redraw
      do @download_plotter.redraw

    #
    # Upload test
    #

    gen_big_data = if String::repeat
      (size)-> " ".repeat size
    else
      (size)->
        str = " "
        res = ""
        loop
          res += str if (count & 1) is 1
          count >>>= 1
          break if count is 0
          str += str
        res

    start_upload: ->
      start_time = do now
      @_upload_abort = put "/test", (gen_big_data 4<<20),
        (err)=>
          @_upload_abort = null
          if err and err.message isnt "Request aborted"
            @upload_errors.push err
            @upload_enable = no
          # repeat test
          do @start_upload unless err
          do @$render
        (loaded, total)=>
          return unless @upload_enable and loaded > 0
          {count, time} = @upload_result[0] or
            time: start_time
            count: 0
          
          chunk = if loaded > count then loaded - count else loaded
          count = loaded

          cur_time = do now
          speed = chunk / (cur_time - time)
          time = cur_time
          
          @upload_result.unshift {time, count, chunk, speed}
          @upload_result[@upload_max_results...] = [] if @upload_result.length > @upload_max_results
          do @$render
          do @upload_plotter.redraw
          
    abort_upload: ->
      if @_upload_abort
        do @_upload_abort

    clear_upload_result: =>
      @upload_result[...] = []
      do @$render
      do @upload_plotter.redraw

    clear_upload_error: (index)=>
      @upload_errors.splice index, 1
      do @$render

    set_upload_enable: (@upload_enable)=>
      if @upload_enable
        do @start_upload
      else
        do @abort_upload
      do @$render

    #
    # Download test
    #

    start_download: ->
      start_time = do now
      @_download_abort = get "/test",
        (err)=>
          @_download_abort = null
          if err and err.message isnt "Request aborted"
            @download_errors.push err
            @download_enable = no
          do @$render
        (loaded, total)=>
          return unless @download_enable and loaded > 0
          {count, time} = @download_result[0] or
            time: start_time
            count: 0
          
          chunk = loaded - count
          count = loaded

          cur_time = do now
          speed = chunk / (cur_time - time)
          time = cur_time
          
          @download_result.unshift {time, count, chunk, speed}
          @download_result[@download_max_results...] = [] if @download_result.length > @download_max_results
          do @$render
          do @download_plotter.redraw
          
    abort_download: ->
      if @_download_abort
        do @_download_abort

    clear_download_result: =>
      @download_result[...] = []
      do @$render
      do @download_plotter.redraw

    clear_download_error: (index)=>
      @download_errors.splice index, 1
      do @$render

    set_download_enable: (@download_enable)=>
      if @download_enable
        do @start_download
      else
        do @abort_download
      do @$render

{
  Tabs
  Tab
  Check
  Row
  Group
  Button
  Alert
} = require "form"

{
  p
  table
  tr
  th
  td
} = require "node"

clear_label =
  en: "Clear results"
  ru: "Очистить результаты"

time_label =
  en: "Time, s"
  ru: "Время, с"

latency_label =
  en: "Latency, ms"
  ru: "Задержка, мс"

size_label =
  en: "Size, bytes"
  ru: "Размер, байт"

speed_label =
  en: "Speed, bytes/s"
  ru: "Скорость, байт/с"

average_label =
  en: "Average"
  ru: "Среднее"

ping_label =
  en: "HTTP-ping"
  ru: "HTTP пинг"

ping_enable_label =
  en: "Test ping"
  ru: "Тестировать пинг"

ping_description =
  en: "This test periodically sends GET requests and measures delay between sending request and receiving response."
  ru: "Этот тест периодически делает GET запросы, измеряя время между отправкой запроса и получением ответа."

ping_error_message =
  en: ({message})-> "Ping test failed with error (#{message})"
  ru: ({message})-> "Ошибка выполнения пинг-теста (#{message})"

download_label =
  en: "HTTP-download"
  ru: "Выгрузка по HTTP"

download_description =
  en: "This test makes GET request, receives continuous data stream and measures number of received bytes per second."
  ru: "Этот тест делает GET запрос и получает непрерывный поток данных, измеряя число полученных байт в секунду."

download_enable_label =
  en: "Test download from server"
  ru: "Тестировать выгрузку с сервера"

download_error_message =
  en: ({message})-> "Download test failed with error (#{message})"
  ru: ({message})-> "Ошибка выполнения теста выгрузки (#{message})"

upload_label =
  en: "HTTP-upload"
  ru: "Зазрузка по HTTP"

upload_description =
  en: "This test makes PUT request, sends continuous data stream and measures number of sent bytes per second."
  ru: "Этот тест делает PUT запрос и отправляет непрерывный поток данных, имеряя число отправленных байт в секунду."

upload_enable_label =
  en: "Test upload to server"
  ru: "Тестировать загрузку на сервер"

upload_error_message =
  en: ({message})-> "Upload test failed with error (#{message})"
  ru: ({message})-> "Ошибка выполнения теста загрузки (#{message})"

Test = (opts)->
  {language} = opts

  ping_result_last = opts.ping_result[0...10]
  
  Tabs
    name: "test"
    style: "error"
    Tab
      label: ping_label[language]
      Row null,
        Group null,
          Check
            label: ping_enable_label[language]
            value: opts.ping_enable
            change: opts.set_ping_enable
          Button
            label: clear_label[language]
            click: opts.clear_ping_result
            disabled: opts.ping_result.length is 0
          do opts.ping_plotter.render
          for error, index in opts.ping_errors
            Alert
              style: "error"
              close: [opts.clear_ping_error, index]
              message: ping_error_message[language] error
          p null, ping_description[language]
        table null,
          tr null,
            th null, time_label[language]
            th null, latency_label[language]
          if ping_result_last.length > 4
            latency = 0
            for {ping, pong} in ping_result_last
              latency += pong - ping
            tr null,
              td null, average_label[language]
              td null, (latency / ping_result_last.length) | 0
          for {ping, pong} in ping_result_last
            tr null,
              td null, (ping / 1000) | 0
              td null, pong - ping
    SpeedTestTab
      plotter: opts.upload_plotter
      language: language
      label: upload_label
      description: upload_description
      enable_label: upload_enable_label
      enable: opts.upload_enable
      set_enable: opts.set_upload_enable
      view_results: opts.upload_view_results
      result: opts.upload_result
      clear_result: opts.clear_upload_result
      errors: opts.upload_errors
      clear_error: opts.clear_upload_error
      error_message: upload_error_message
    SpeedTestTab
      plotter: opts.download_plotter
      language: language
      label: download_label
      description: download_description
      enable_label: download_enable_label
      enable: opts.download_enable
      set_enable: opts.set_download_enable
      view_results: opts.download_view_results
      result: opts.download_result
      clear_result: opts.clear_download_result
      errors: opts.download_errors
      clear_error: opts.clear_download_error
      error_message: download_error_message

SpeedTestTab = (opts)->
  {language} = opts
  result_last = opts.result[0...opts.view_results]
  
  Tab
    label: opts.label[language]
    Row null,
      Group null,
        Check
          label: opts.enable_label[language]
          value: opts.enable
          change: opts.set_enable
        Button
          label: clear_label[language]
          click: opts.clear_result
          disabled: opts.result.length is 0
        do opts.plotter.render
        for error, index in opts.errors
          Alert
            style: "error"
            close: [opts.clear_error, index]
            message: opts.error_message[language] error
        p null, opts.description[language]
      table null,
        tr null,
          th null, time_label[language]
          th null, size_label[language]
          th null, speed_label[language]
        if result_last.length > 4
          avg_chunk = 0
          avg_speed = 0
          for {chunk, speed} in result_last
            avg_chunk += chunk
            avg_speed += speed
          tr null,
            td null, average_label[language]
            td null, (avg_chunk / result_last.length) | 0
            td null, (avg_speed * 1000 / result_last.length) | 0
        for {time, chunk, speed} in result_last
          tr null,
            td null, (time / 1000) | 0
            td null, chunk
            td null, (speed * 1000) | 0

@views = [
  name: "test"
  view: Test
  title:
    en: "Test"
    ru: "Тестирование"
]
