@commit_label =
  en: "Apply"
  ru: "Применить"

@revert_label =
  en: "Revert"
  ru: "Откатить"

@reset_label =
  en: "Reset"
  ru: "Сбросить"

@username_label =
  en: "Username"
  ru: "Имя пользователя"

@password_label =
  en: "Password"
  ru: "Пароль"

@show_password_label =
  en: "Show password"
  ru: "Показать пароль"

@ip_label =
  en: "IP-address"
  ru: "IP-адрес"

@gateway_label =
  en: "Gateway"
  ru: "Шлюз"

@netmask_label =
  en: "Netmask"
  ru: "Маска сети"

@hostname_label =
  en: "Hostname"
  ru: "Имя хоста"

not_an_integer =
  en: "Integer number required"
  ru: "Требуется целое число"

integer_regexp = /^(:?[-]?[1-9][0-9]*|0)$/

@check_int = (value)->
  not_an_integer unless integer_regexp.test value
  null

not_a_number =
  en: "The number required"
  ru: "Требуется число"

number_regexp = /^(?:[-]?[1-9][0-9]*|0)(?:[.][0-9]+)$/

@check_num = (value)->
  num_a_number unless number_regexp.test value
  null

too_short =
  en: do ->
    s = (n, f)-> "At least #{n} #{f} required."
    [
      (n)-> s n, "character"
      (n)-> s n, "characters"
    ]
  ru: do ->
    s = (n, f)-> "Требуется по крайней мере #{n} #{f}."
    [
      (n)-> s n, "символ"
      (n)-> s n, "символа"
      (n)-> s n, "символов"
    ]

[too_short, too_long] = do ->
  en_f = [
    "character"
    "characters"
  ]
  ru_f = [
    "символ"
    "символа"
    "символов"
  ]
  [
    (len, form)->
      en: "At least #{len} #{en_f[form]} required."
      ru: "Требуется по крайней мере #{len} #{ru_f[form]}."
  ,
    (len, form)->
      en: "Less than or exactly #{len} #{en_f[form]} required."
      ru: "Требуется не более чем #{len} #{ru_f[form]}."
  ]

@check_length = (str, from, to, plural)->
  return too_short from, plural from if from? and str.length < from
  return too_long to, plural to if to? and str.length > to
  null

too_small = (v)->
  en: "The value must be greater or equals to #{v}"
  ru: "Значение должно быть больше либо равно #{v}"

too_big = (v)->
  en: "The value must be less or equals to #{v}"
  ru: "Значение должно быть меньше либо равно #{v}"

@check_range = (val, from, to)->
  return too_small from if from? and val < from
  return too_big to if to? and val > to
  null

###
@fix_domain = (domain)->
  domain.replace /[^0-9a-zA-Z\.\_\-]/g, ""
###

invalid_ip =
  en: "IP-address is invalid"
  ru: "Не корректный IP-адрес"

invalid_ip_octet =
  en: "Each octet of IP-address must be in range from 1 to 255"
  ru: "Каждый октет IP-адреса должен быть в диапазоне от 1 до 255"

ip_regexp = /^(\d+)\.(\d+)\.(\d+)\.(\d+)$/

###
@fix_ip = (ip)->
  (octet.replace /^0+([1-9])/g, "$1" for octet in (ip.replace /[^0-9\.]/g, "").split /\./)[..3].join "."
###

@check_ip = check_ip = (ip)->
  return invalid_ip unless ip?
  octets = ip.match ip_regexp
  return invalid_ip unless octets?
  do octets.shift
  return invalid_ip_octet for octet in octets when not (0 < (octet|0) < 256)
  null

invalid_gateway =
  en: "Gateway is invalid"
  ru: "Не корректный шлюз"

invalid_gateway_octet =
  en: "Each octet of gateway must be in range from 1 to 255"
  ru: "Каждый октет шлюза должен быть в диапазоне от 1 до 255"

@check_gateway = check_gateway = (gateway)->
  return invalid_gateway unless gateway?
  octets = gateway.match ip_regexp
  return invalid_gateway unless octets?
  do octets.shift
  return invalid_gateway_octet for octet in octets when not (0 < (octet|0) < 256)
  null

valid_netmask_octets =
  0b11111111: 3
  0b11111110: 2
  0b11111100: 2
  0b11111000: 2
  0b11110000: 2
  0b11100000: 2
  0b11000000: 2
  0b10000000: 2
  0b00000000: 1

valid_netmask_octets_text = Object.keys valid_netmask_octets

invalid_netmask =
  en: "Netmask is invalid"
  ru: "Не корректная маска сети"

invalid_netmask_octet =
  en: "Each octet of netmask must be one of #{valid_netmask_octets_text}"
  ru: "Каждый октет маски сети должен быть одним из #{valid_netmask_octets_text}"

invalid_netmask_order =
  en: "Order of octets in netmask is invalid"
  ru: "Не верный порядок октетов в маске сети"

@check_netmask = check_netmask = (netmask)->
  return invalid_netmask unless netmask?
  octets = netmask.match ip_regexp
  return invalid_netmask unless octets?
  do octets.shift
  return invalid_netmask_octet for octet in octets when not valid_netmask_octets[octet]
  order_ = 3
  for octet in octets
    octet |= 0
    order = valid_netmask_octets[octet]
    if order > order_ or (order is order_ and order is 2)
      return invalid_netmask_order
    order_ = order
  null

invalid_netconf =
  en: "IP-address and gateway in different networks"
  ru: "IP-адрес и шлюз в разных подсетях"

@check_netconf = (ip, gateway, netmask)->
  ip_error = check_ip ip
  gateway_error = check_gateway gateway
  netmask_error = check_netmask netmask

  unless ip_error? or gateway_error? or netmask_error?
    ip_octets = ip.match ip_regexp
    gateway_octets = gateway.match ip_regexp
    netmask_octets = netmask.match ip_regexp
    for i in [1..4]
      mask = netmask_octets[i]|0
      if ((ip_octets[i]|0) & mask) isnt ((gateway_octets[i]|0) & mask)
        ip_error = gateway_error = netmask_error = invalid_netconf
        break
  
  [ip_error, gateway_error, netmask_error]

{round} = Math

@dbm_to_percent = (val)->
  if val <= -100
    0
  else
    if val >= -50
      100
    else
      round 2 * (val + 100)
