{
  get
  put
} = require "req"

{
  check_int
  check_num
  check_range
} = require "common"

@Param = (Base)->
  class Param extends Base
    constructor: ->
      super
      @_param_observe_interval = null
      @_param_observe_groups = {}
      do @$param_load

    $param_observe_update: ->
      @_param_observe_interval = null
      @_param_observe_params = []
      for group, {state, interval, params} of @_param_observe_groups when state
        if not @_param_observe_interval? or @_param_observe_interval > interval
          @_param_observe_interval = interval
        @_param_observe_params.push param for param in params
      if @_param_observe_interval? and not @_param_observe_timer?
        @_param_observe_timer = setTimeout @$param_observe_trigger, @_param_observe_interval
      if not @_param_observe_interval? and @_param_observe_timer?
        clearTimeout @_param_observe_timer
        @_param_observe_timer = null

    $param_observe_trigger: =>
      if @_param_observe_params?.length
        @$param_get @_param_observe_params, =>
          if @_param_observe_interval?
            @_param_observe_timer = setTimeout @$param_observe_trigger, @_param_observe_interval
          do @$render

    $param_observe: (group, interval, params)->
      @_param_observe_groups[group] = {interval, params, state: off}
    
      @["start_observe_#{group}"] = =>
        @_param_observe_groups[group].state = on
        do @$param_observe_update
      @["stop_observe_#{group}"] = ->
        @_param_observe_groups[group].state = off
        do @$param_observe_update

    param_loaded: ->

    $param_load: ->
      get "/param", (err, descs)=>
        if descs?
          @params = {}
          for desc in descs
            @params[desc.name] = desc
            if "setable" in desc.flag and not @["set_#{desc.name}"]?
              do ({name} = desc, o = @)->
                o["set_#{name}"] = (value)->
                  @$param_change "#{name}": value
          do @param_loaded
          do @$render

    $param_get: (params, result)->
      put "/param", params, (err, values)=>
        if values?
          for name, index in params
            @[name] = values[index]
          result? err

    $param_set: (params, result)->
      values = {}
      for name in params when @["_#{name}"]?
        values[name] = @[name]
      put "/param", values, (err, res)=>
        @$param_proper params unless err
        result? err

    $param_group: (group, params)->
      for name in params
        @params[name]?.group = group

      @["update_#{group}"] = ->
        @["#{group}_changed"] = @$param_changed params
        @["#{group}_valid"] = @$param_valid params
        @["#{group}_custom"] = @$param_custom params
        do @$render

      @["load_#{group}"] = ->
        @$param_get params, =>
          do @["update_#{group}"]
          do @$render

      @["commit_#{group}"] = ->
        @$param_set params, =>
          do @["update_#{group}"]
          do @$render

      @["revert_#{group}"] = ->
        @$param_revert params
        do @update_settings
        do @$render

      @["reset_#{group}"] = ->
        @$param_reset params
        do @$render

    num_min =
      uint:
        1: [0, (1<<8)-1]
        2: [0, (1<<16)-1]
        4: [0, (1<<30)-1] # ! js restrictions
        8: [0, (1<<30)-1]
      sint:
        1: [-(1<<7), (1<<7)-1]
        2: [-(1<<15), (1<<15)-1]
        4: [-(1<<30), (1<<30)-1] # ! js restrictions
        8: [-(1<<30), (1<<30)-1]

    validate = (desc, value)->
      # check special type
      error = switch desc.type
        when "uint", "sint"
          check_int value
        when "real"
          check_num value
      return error if error?
      # check range
      error = switch desc.type
        when "uint", "sint", "real"
          check_range value,
            if desc.min? then desc.min else num_range[desc.type][desc.size][0]
            if desc.max? then desc.max else num_range[desc.type][desc.size][1]
      return error if error?
      null

    $param_change: (params)->
      groups = []
      for name, value of params
        @["_#{name}"] = @[name] unless @["_#{name}"]?
        if @["fix_#{name}"]?
          value = @["fix_#{name}"] value
        @[name] = value
        desc = @params?[name]
        if desc?
          @["#{name}_error"] = validate desc, value
        if @["val_#{name}"]?
          do @["val_#{name}"]
        if @["update_#{desc?.group}"]?
          groups.push desc.group
        delete @["_#{name}"] if @["_#{name}"] is @[name]
      for group in groups
        do @["update_#{group}"]

    $param_revert: (params)->
      values = {}
      for name in params when @["_#{name}"]?
        values[name] = @["_#{name}"]
      @$param_change values

    $param_proper: (params)->
      for name in params when @["_#{name}"]?
        delete @["_#{name}"]

    $param_reset: (params)->
      if @params?
        values = {}
        for name in params when @params[name]?.def?
          values[name] = @params[name].def
        @$param_change values

    $param_changed: (params)->
      return yes for name in params when @["_#{name}"]?
      no

    $param_custom: (params)->
      if @params?
        return yes for name in params when @params[name]?.def? and @[name] isnt @params[name]?.def
        return no

    $param_valid: (params)->
      return no for name in params when @["#{name}_error"]?
      return yes
