{get, put} = require "req"
{addHandler, delHandler} = require "dom"

#
# Base controller
#

class Base
  languages:
    en:
      title: "English"
      plural: (n)->
        n |= 0
        # nplurals=2; plural=(n != 1);
        if n isnt 1 then 1 else 0
    ru:
      title: "Русский"
      plural: (n)->
        n |= 0
        # nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
        switch
          when n % 10 is 1 and n % 100 isnt 11 then 0
          when n % 10 >= 2 and n % 10 <= 4 and (n % 100 < 10 or n % 100 >= 20) then 1
          else 2

  $language: ->
    localStorage?.language or (navigator?.userLanguage or navigator?.language or "en").replace /[\-\_].+$/, ""

  constructor: ->
    do @switch_language

  switch_language: (language)->
    localStorage?.language = @language = if language? and language of @languages then language else do @$language
    @plural = @languages[@language].plural

  $_resize: =>
    do @$resize

  $resize: ->

  $mount: ->
    addHandler "resize", @$_resize
    do @$resize

  $patch: ->

{Param} = require "param"
Base = Param Base

#
# Modules
#

views = []
tags = []

for mod in require "modules"
  Base = mod.Ctrl Base
  views.push view for view in mod.views if mod.views?.length
  tags.push tag for tag in mod.tags if mod.tags?.length

#
# Controller
#

class @Main extends Base
  constructor: ->
    super
    @select_view 0

  select_view: (view)=>
    @["hide_#{@view_name}"]?()
    @view = view
    @view_name = views[@view]?.name
    @["show_#{@view_name}"]?()
    do @$render

#
# View
#

{
  body
  nav
  div
  a
  input
  label
} = require "node"

view_style =
  opacity: 0
  "margin-top": "-100px"
  transition: "opacity 1s, margin-top 1s"
  delayed:
    opacity: 1
    "margin-top": "0px"
  remove:
    opacity: 0
    "margin-top": "100px"

@Root = (opts)->
  body null,
    nav null,
      for tag, id in tags
        tag opts
      # responsive
      input
        attrs:
          id: "bmenu"
          type: "checkbox"
        class:
          show: on
      label
        attrs:
          for: "bmenu"
        class:
          burger: on
          pseudo: on
          button: on
          toggle: on
        "..."
      div
        class:
          menu: on
        for view, id in views
          a
            key: id
            class:
              button: on
              pseudo: opts.view isnt id
            attrs:
              href: "#"
            on:
              click: do (id)-> -> opts.select_view id
            view.title[opts.language]
    div
      class:
        main: on
      div
        key: opts.view
        style: view_style
        views[opts.view].view opts
