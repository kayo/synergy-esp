{
  a
} = require "node"

{
  dbm_to_percent
} = require "common"

@Ctrl = (Base)->
  class DevMon extends Base
    constructor: ->
      super
      @$param_observe "devmon", 5000, [
        "free_heap"
        "st_rssi"
      ]
      do @start_observe_devmon

free_heap_label =
  en: (free)-> "Free Heap: #{free} Kb"
  ru: (free)-> "Свободно: #{free} Кб"

rssi_label =
  en: (rssi)-> "RSSI: #{rssi}%"
  ru: (rssi)-> "Сигнал: #{rssi}%"

@tags = [
  ({free_heap, st_rssi, language})->
    if free_heap? or st_rssi?
      a
        class:
          label: on
          success: on
        free_heap_label[language] (free_heap / 1024).toFixed 3
        " "
        rssi_label[language] dbm_to_percent st_rssi
]
