{
  div
  p
  a
  strong
} = require "node"

{
  Entry
} = require "form"

@Ctrl = (Base)->
  Base

fw_device_label =
  en: "Device name"
  ru: "Название устройства"

fw_version_label =
  en: "Firmware version"
  ru: "Версия прошивки"

content =
  en: div null,
    p null,
      "Revolutionary firmware for "
      strong null, "ESP8266"
      "."
    p null,
      "To get more information see "
      a attrs: href: "https://bitbucket.org/kayo/synergy-esp", "bitbucket.org:kayo/synergy-esp"
  ru: div null,
    p null,
      "Революционная прошивка для "
      strong null, "ESP8266"
      "."
    p null,
      "Подробности на странице проекта "
      a attrs: href: "http://bitbucket.org/kayo/synergy-esp", "bitbucket.org:kayo/synergy-esp"

Info = (opts)->
  {language} = opts
  
  div null,
    Entry
      label: fw_device_label[language]
      value: opts.params?.fw_device?.def
      readonly: yes
    Entry
      label: fw_version_label[language]
      value: opts.params?.fw_version?.def
      readonly: yes
    content[language]

@views = [
  name: "info"
  view: Info
  title:
    en: "Info"
    ru: "Инфо"
]
