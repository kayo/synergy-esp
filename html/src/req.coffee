{parse, stringify} = JSON

parseHeaders = (hs)->
  h = {}
  for he in hs.split /\r?\n/
    [f, v] = he.split /:\s*/
    h[f] = v if v?
  h

request = (method, url, headers, body, result, progress)->
  xhr = new XMLHttpRequest
  xhr.open method, url, yes
  
  if "object" is typeof body
    xhr.setRequestHeader "Content-Type", "application/json"
    try
      body = stringify body
    catch error
      result error
      return
  
  if headers?
    for field, value in headers
      xhr.setRequestHeader field, value
  
  if progress?
    if body?
      xhr.upload.onprogress = ({loaded, total})->
        progress loaded, total, yes
    xhr.onprogress = ({loaded, total})->
      progress loaded, total

  aborted = no
  sended = no
  xhr.onreadystatechange = ->
    if sended and not aborted and xhr.readyState is 4
      xhr.onreadystatechange = null
      result null, xhr.status, xhr.statusText, (parseHeaders do xhr.getAllResponseHeaders), xhr.responseText

  xhr.onabort = ->
    result new Error "Request aborted"
  
  try
    xhr.send body
    sended = yes
  catch error
    xhr.onreadystatechange = null
    process.nextTick -> result error

  ->
    aborted = yes
    xhr.abort?()

response = (result)->
  (error, status, message, headers, body)->
    if error?
      result error
      return
    
    if status < 200 or status > 299
      result new Error "Invalid status: #{message}"
      return
    
    if body?.length
      if /json/.test headers["Content-Type"]
        try
          body = parse body
        catch error
          result error
          return

    result null, body

@get = (url, result, progress)-> request "GET", url, null, null, (response result), progress
@put = (url, data, result, progress)-> request "PUT", url, null, data, (response result), progress
