#
# Controller
#

{get} = require "req"

@Ctrl = (Base)->
  class Radio extends Base
    constructor: ->
      super
      @ap_show_password = no
      @st_show_password = no
      @st_use_bssid = no
      @st_ssid_index = -1
      @st_scan_list = []

    param_loaded: ->
      @$param_group "radio", [
        "op_mode"
        "phy_mode"
        "ap_ssid"
        "ap_password"
        "ap_channel"
        "ap_authmode"
        "ap_hidden"
        "ap_maxconn"
        "ap_beacon"
        "ap_ip"
        "ap_gateway"
        "ap_netmask"
        "ap_dhcps"
        "ap_dhcps_from"
        "ap_dhcps_to"
        "st_hostname"
        "st_ssid"
        "st_password"
        "st_dhcpc"
        "st_ip"
        "st_gateway"
        "st_netmask"
      ]
      do @load_radio if @view_name is "radio"
      super

    show_radio: ->
      @load_radio?()
      do @trigger_scan

    trigger_scan: ->
      get '/scan', (err, res)=>
        if res?.length
          @st_scan_list = res
          do @$render
    
    val_ap_password: ->
      @ap_password_error = check_length @ap_password, 8, null, @plural if @ap_authmode isnt "open" and @ap_password?.length > 0

    set_ap_show_password: (@ap_show_password)->
      do @$render

    val_ap_ip: ->
      [@ap_ip_error, @ap_gateway_error, @ap_netmask_error] = check_netconf @ap_ip, @ap_gateway, @ap_netmask

    val_ap_gateway: -> do @val_ap_ip
    val_ap_netmask: -> do @val_ap_ip

    val_ap_dhcps: ->
      [@ap_dhcps_from_error, ...] = check_netconf @ap_dhcps_from, @ap_gateway, @ap_netmask
      [@ap_dhcps_to_error, ...] = check_netconf @ap_dhcps_to, @ap_gateway, @ap_netmask

    val_ap_dhcps_from: -> do @val_ap_dhcps
    val_ap_dhcps_to: -> do @val_ap_dhcps

    set_st_use_bssid: (@st_use_bssid)->
      do @select_st_ssid

    select_st_ssid: (@st_ssid_index = @st_ssid_index)->
      if @st_ssid_index > -1 # Check AP index and set
        @set_st_ssid @st_scan_list[@st_ssid_index][if @st_use_bssid then "bssid" else "ssid"]
      else
        do @$render

    custom_st_ssid: (st_ssid)->
      @st_ssid_index = -1 # Reset AP index because we use custom network
      @set_st_ssid st_ssid

    val_st_password: ->
      check_length @st_password, 8, null, @plural if @st_ssid?.length > 0 and @st_password?.length > 0

    set_st_show_password: (@st_show_password)->
      do @$render

    val_st_ip: ->
      [@st_ip_error, @st_gateway_error, @st_netmask_error] = check_netconf @st_ip, @st_gateway, @st_netmask

    val_st_gateway: -> do @val_st_ip
    val_st_netmask: -> do @val_st_ip

#
# View
#

{
  Row
  Tabs
  Tab
  Group
  Check
  Entry
  Select
  Button
} = require "form"

{
  commit_label
  revert_label
  reset_label
  password_label
  show_password_label
  ip_label
  gateway_label
  netmask_label
  hostname_label
  check_length
  check_netconf
  dbm_to_percent
} = require "common"

basic_settings_label =
  en: "Basic"
  ru: "Основные"

advanced_settings_label =
  en: "Advanced"
  ru: "Продвинутые"

network_settings_label =
  en: "Network"
  ru: "Сеть"

dhcp_settings_label =
  en: "DHCP"
  ru: "DHCP"

settings_label =
  en: "Wireless network"
  ru: "Беспроводная сеть"

op_mode_label =
  en: "Operation mode"
  ru: "Режим работы"

op_mode_options =
  en: [
    value: "off"
    label: "Disabled"
  ,
    value: "sta"
    label: "Station"
  ,
    value: "ap"
    label: "Access-Point"
  ,
    value: "stap"
    label: "Station and Access-Point"
  ]
  ru: [
    value: "off"
    label: "Отключено"
  ,
    value: "sta"
    label: "Станция"
  ,
    value: "ap"
    label: "Точка доступа"
  ,
    value: "stap"
    label: "Станция и точка доступа"
  ]

phy_mode_label =
  en: "Physical mode"
  ru: "Физический режим"

phy_mode_options = [
  value: "b"
  label: "802.11b"
,
  value: "g"
  label: "802.11g"
,
  value: "n"
  label: "802.11n"
]

ap_settings_label =
  en: "Access-Point"
  ru: "Точка доступа"

ap_authmode_label =
  en: "Auth mode"
  ru: "Режим авторизации"

ap_authmode_options =
  en: [
    value: "open"
    label: "Open network"
  ,
    value: "wpa_psk"
    label: "WPA-PSK encryption"
  ,
    value: "wpa2_psk"
    label: "WPA2-PSK encryption"
  ,
    value: "wpa_wpa2_psk"
    label: "WPA/WPA2-PSK encryption"
  ]
  ru: [
    value: "open"
    label: "Открытая сеть"
  ,
    value: "wpa_psk"
    label: "Шифрование WPA-PSK"
  ,
    value: "wpa2_psk"
    label: "Шифрование WPA2-PSK"
  ,
    value: "wpa_wpa2_psk"
    label: "Шифрование WPA/WPA2-PSK"
  ]

ap_hidden_label =
  en: "Hidden network"
  ru: "Скрытая сеть"

ap_channel_label =
  en: "Channel"
  ru: "Канал"

ap_channel_options = (value: n, label: n for n in [1..13])

ap_beacon_label =
  en: "Beacon interval, ms"
  ru: "Интервал маяка, мс"

ap_maxconn_label =
  en: "Connections limitation"
  ru: "Ограничение подключений"

ap_dhcps_label =
  en: "Enable DHCP-server"
  ru: "Включить сервер DHCP"

ap_dhcps_from_label =
  en: "DHCP lease start IP"
  ru: "Начальный IP-адрес аренды DHCP"

ap_dhcps_to_label =
  en: "DHCP lease end"
  ru: "Конечный IP-адрес аренды DHCP"

st_settings_label =
  en: "Station"
  ru: "Станция"

st_ssid_label =
  en: "Access point"
  ru: "Точка доступа"

st_ssid_select =
  en: "Select network"
  ru: "Выбрать сеть"

st_use_bssid_label =
  en: "Use BSSID"
  ru: "Использовать BSSID"

st_dhcpc_label =
  en: "Enable DHCP-client"
  ru: "Включить клиент DHCP"

ssid_label =
  en: "Network SSID"
  ru: "SSID сети"

bssid_label =
  en: "Network BSSID"
  ru: "BSSID сети"

Radio = (opts)->
  {language, params} = opts
  
  networks = [
    value: -1
    label: st_ssid_select[language]
  ]
  
  for {ssid, bssid, rssi, auth, channel}, index in opts.st_scan_list
    networks.push
      value: index
      label: "#{ssid} (#{auth} #{bssid} ##{channel} #{dbm_to_percent rssi}%)"
  
  Tabs
    name: "radio"
    style: "error"
    Tab
      label: settings_label[language]
      Select
        id: "op-mode"
        label: op_mode_label[language]
        options: op_mode_options[language]
        value: opts.op_mode
        self: opts
        change: opts.set_op_mode
        disabled: not opts.op_mode?
        style: "success" if opts._op_mode?
      Select
        id: "phy-mode"
        label: phy_mode_label[language]
        options: phy_mode_options
        value: opts.phy_mode
        self: opts
        change: opts.set_phy_mode
        disabled: not opts.phy_mode?
        style: "success" if opts._phy_mode?
    Tab
      label: ap_settings_label[language]
      Tabs
        name: "ap"
        style: "pseudo"
        Tab
          label: basic_settings_label[language]
          Entry
            label: ssid_label[language]
            value: opts.ap_ssid
            self: opts
            change: opts.set_ap_ssid
            disabled: not opts.ap_ssid?
            style: switch
              when opts.ap_ssid_error then "error"
              when opts._ap_ssid? then "success"
            tooltip: opts.ap_ssid_error?[language]
            maxlength: params?.ap_ssid?.size
          Select
            id: "auth-mode"
            label: ap_authmode_label[language]
            options: ap_authmode_options[language]
            value: opts.ap_authmode
            self: opts
            change: opts.set_ap_authmode
            disabled: not opts.ap_authmode?
            style: "success" if opts._ap_authmode?
          Entry
            label: password_label[language]
            password: not opts.ap_show_password
            value: opts.ap_password
            self: opts
            change: opts.set_ap_password
            disabled: not opts.ap_password?
            style: switch
              when opts.ap_password_error then "error"
              when opts._ap_password? then "success"
            tooltip: opts.ap_password_error?[language]
            maxlength: params?.ap_password?.size
          Check
            label: show_password_label[language]
            value: opts.ap_show_password
            self: opts
            change: opts.set_ap_show_password
            disabled: not opts.ap_password?
        Tab
          label: advanced_settings_label[language]
          Check
            label: ap_hidden_label[language]
            value: opts.ap_hidden
            checked: "hidden"
            unchecked: "visible"
            self: opts
            change: opts.set_ap_hidden
            disabled: not opts.ap_hidden?
            style: "success" if opts._ap_hidden?
          Select
            label: ap_channel_label[language]
            value: opts.ap_channel
            self: opts
            change: opts.set_ap_channel
            disabled: not opts.ap_channel?
            options: ap_channel_options
            style: "success" if opts._ap_channel?
          Entry
            label: ap_beacon_label[language]
            value: opts.ap_beacon
            self: opts
            change: opts.set_ap_beacon
            disabled: not opts.ap_beacon?
            style: switch
              when opts.ap_beacon_error then "error"
              when opts._ap_beacon? then "success"
            tooltip: opts.ap_beacon_error?[language]
            maxlength: 6
          Entry
            label: ap_maxconn_label[language]
            value: opts.ap_maxconn
            self: opts
            change: opts.set_ap_maxconn
            disabled: not opts.ap_maxconn?
            style: switch
              when opts.ap_maxconn_error then "error"
              when opts._ap_maxconn? then "success"
            tooltip: opts.ap_maxconn_error?[language]
            maxlength: 2
        Tab
          label: network_settings_label[language]
          Entry
            label: ip_label[language]
            value: opts.ap_ip
            self: opts
            change: opts.set_ap_ip
            disabled: not opts.ap_ip?
            style: switch
              when opts.ap_ip_error then "error"
              when opts._ap_ip? then "success"
            tooltip: opts.ap_ip_error?[language]
            maxlength: 16
          Entry
            label: gateway_label[language]
            value: opts.ap_gateway
            self: opts
            change: opts.set_ap_gateway
            disabled: not opts.ap_gateway?
            style: switch
              when opts.ap_gateway_error then "error"
              when opts._ap_gateway? then "success"
            tooltip: opts.ap_gateway_error?[language]
            maxlength: 16
          Entry
            label: netmask_label[language]
            value: opts.ap_netmask
            self: opts
            change: opts.set_ap_netmask
            disabled: not opts.ap_netmask?
            style: switch
              when opts.ap_netmask_error then "error"
              when opts._ap_netmask? then "success"
            tooltip: opts.ap_netmask_error?[language]
            maxlength: 16
        Tab
          label: dhcp_settings_label[language]
          Check
            label: ap_dhcps_label[language]
            value: opts.ap_dhcps
            checked: "enabled"
            unchecked: "disabled"
            self: opts
            change: opts.set_ap_dhcps
            disabled: not opts.ap_dhcps?
            style: "success" if opts._ap_dhcps?
          Entry
            label: ap_dhcps_from_label[language]
            value: opts.ap_dhcps_from
            self: opts
            change: opts.set_ap_dhcps_from
            disabled: not opts.ap_dhcps_from? or opts.ap_dhcps is "disabled"
            style: switch
              when opts.ap_dhcps_from_error then "error"
              when opts._ap_dhcps_from? then "success"
            tooltip: opts.ap_dhcps_from_error?[language]
            maxlength: 16
          Entry
            label: ap_dhcps_to_label[language]
            value: opts.ap_dhcps_to
            self: opts
            change: opts.set_ap_dhcps_to
            disabled: not opts.ap_dhcps_to? or opts.ap_dhcps is "disabled"
            style: switch
              when opts.ap_dhcps_to_error then "error"
              when opts._ap_dhcps_to? then "success"
            tooltip: opts.ap_dhcps_to_error?[language]
            maxlength: 16
    Tab
      label: st_settings_label[language]
      Tabs
        name: "st"
        style: "pseudo"
        Tab
          label: basic_settings_label[language]
          Select
            id: "st-ssid"
            label: st_ssid_label[language]
            options: networks
            value: opts.st_ssid_index
            self: opts
            change: opts.select_st_ssid
            disabled: networks.length is 1
          Check
            label: st_use_bssid_label[language]
            value: opts.st_use_bssid
            self: opts
            change: opts.set_st_use_bssid
            disabled: not opts.st_ssid?
          Entry
            label: (if opts.st_use_bssid then bssid_label else ssid_label)[language]
            value: opts.st_ssid
            self: opts
            change: opts.custom_st_ssid
            disabled: not opts.st_ssid?
            maxlength: params?.st_ssid?.size
            style: switch
              when opts.st_ssid_error then "error"
              when opts._st_ssid? then "success"
            tooltip: opts.st_ssid_error?[language]
          Entry
            label: password_label[language]
            password: not opts.st_show_password
            value: opts.st_password
            self: opts
            change: opts.set_st_password
            disabled: not opts.st_password?
            style: switch
              when opts.st_password_error then "error"
              when opts._st_password? then "success"
            tooltip: opts.st_password_error?[language]
            maxlength: params?.st_password?.size
          Check
            label: show_password_label[language]
            value: opts.st_show_password
            self: opts
            change: opts.set_st_show_password
            disabled: not opts.st_password?
        Tab
          label: network_settings_label[language]
          Check
            label: st_dhcpc_label[language]
            value: "enabled"
            checked: "enabled"
            unchecked: "disabled"
            self: opts
            change: opts.set_st_dhcpc
            disabled: not opts.st_dhcpc?
            style: "success" if opts._st_dhcps?
          Entry
            label: ip_label[language]
            value: opts.st_ip
            self: opts
            change: opts.set_st_ip
            disabled: not opts.st_ip? or opts.st_dhcpc is "enabled"
            style: switch
              when opts.st_ip_error then "error"
              when opts._st_ip? then "success"
            tooltip: opts.st_ip_error?[language]
            maxlength: 16
          Entry
            label: gateway_label[language]
            value: opts.st_gateway
            self: opts
            change: opts.set_st_gateway
            disabled: not opts.st_gateway? or opts.st_dhcpc is "enabled"
            style: switch
              when opts.st_gateway_error then "error"
              when opts._st_gateway? then "success"
            tooltip: opts.st_gateway_error?[language]
            maxlength: 16
          Entry
            label: netmask_label[language]
            value: opts.st_netmask
            self: opts
            change: opts.set_st_netmask
            disabled: not opts.st_netmask? or opts.st_dhcpc is "enabled"
            style: switch
              when opts.st_netmask_error then "error"
              when opts._st_netmask? then "success"
            tooltip: opts.st_netmask_error?[language]
            maxlength: 16
          Entry
            label: hostname_label[language]
            value: opts.st_hostname
            self: opts
            change: opts.set_st_hostname
            disabled: not opts.st_hostname?
            maxlength: params?.st_hostname?.size
            style: switch
              when opts.st_hostname_error then "error"
              when opts._st_hostname? then "success"
            tooltip: opts.st_hostname_error?[language]
    Group
      controls: yes
      Button
        style: "primary"
        label: commit_label[language]
        disabled: not opts.radio_changed or not opts.radio_valid
        self: opts
        click: opts.commit_radio
      Button
        style: "warning"
        label: revert_label[language]
        disabled: not opts.radio_changed
        self: opts
        click: opts.revert_radio
      Button
        style: "error"
        label: reset_label[language]
        disabled: not opts.radio_custom
        self: opts
        click: opts.reset_radio

@views = [
  name: "radio"
  view: Radio
  title:
    en: "Radio"
    ru: "Радио"
]
