if window? and (doc = (win = window).document)?

  ###
    @brief Get element offset

    @param [DOMNode] elm The target element
    @return [Object] {x, y} The element offset
  ###
  @getOffset = (elm, depth = Infinity)->
    pos =
      x: 0
      y: 0
    while elm? and depth > 0
      pos.x += elm.offsetLeft
      pos.y += elm.offsetTop
      elm = elm.offsetParent
      depth--
    pos

  ###
    @brief Get inner size

    @param elm [DOMNode] The optional element
    @return [Object] {w, h} The inner size
  ###
  @getInner = (elm)->
    if elm?
      w: elm.innerWidth or elm.clientWidth
      h: elm.innerHeight or elm.clientHeight
    else
      {documentElement} = doc
      [body] = doc.getElementsByTagName "body"
      w: win.innerWidth or documentElement.clientWidth or body?.clientWidth
      h: win.innerHeight or documentElement.clientHeight or body?.clientHeight

  ###
    @brief Get outer size

    @param elm [DOMNode] The target element
    @return [Object] {w, h} The outer size
  ###
  @getOuter = (elm)->
    w: elm?.innerWidth or elm?.clientWidth
    h: elm?.innerHeight or elm?.clientHeight

  ###
    @brief Get global scrolling position

    @param [DOMNode] elm The optional element
    @return [Object] {x, y, X, Y}
  ###
  @getScroll = (elm)->
    if elm?
      x: (elm.scrollLeft or 0) - (elm.clientLeft or 0)
      y: (elm.scrollLeft or 0) - (elm.clientLeft or 0)
      X: elm.scrollWidth - elm.clientWidth
      Y: elm.scrollHeight - elm.clientHeight
    else
      {documentElement} = doc
      [body] = doc.getElementsByTagName "body"
      # current scrolling position
      x: (window.pageXOffset or documentElement.scrollLeft or body?.scrollLeft or 0) - (documentElement.clientLeft or body?.clientLeft or 0)
      y: (window.pageYOffset or documentElement.scrollTop or body?.scrollTop or 0) - (documentElement.clientTop or body?.clientTop or 0)
      # maximum scrolling position
      X: (documentElement.scrollWidth or body?.scrollWidth) - (documentElement.clientWidth or body?.clientWidth)
      Y: (documentElement.scrollHeight or body?.scrollHeight) - (documentElement.clientHeight or body?.clientHeight)

  ###
    @brief Set scrolling position

    @param [Object] {x, y} The target position
    @param [DOMNode] elm The optional element
  ###
  @setScroll = ({x, y}, elm)->
    if elm?
      elm.scrollLeft = x if x?
      elm.scrollTop = y if y?
    else
      {documentElement, body} = doc
      if x?
        body?.scrollLeft = x
        documentElement?.scrollLeft = x
      if y?
        body?.scrollTop = y
        documentElement?.scrollTop = y
    return

  ###
    @brief Add event handler

    @param [String] ev The event name
    @param [Function] fn The handler function
    @param [DOMNode] el The optional element
  ###
  @addHandler = switch "function"
    when typeof doc.addEventListener
      (ev, fn, el = win)-> el.addEventListener ev, fn, no
    when typeof doc.attachEvent
      (ev, fn, el = win)-> el.attachEvent "on#{ev}", fn
    else
      ->

  ###
    @brief Delete event handler

    @param [String] ev The event name
    @param [Function] fn The handler function
    @param [DOMNode] el The optional element
  ###
  @delHandler = switch "function"
    when typeof doc.removeEventListener
      (ev, fn, el = win)-> el.removeEventListener ev, fn, no
    when typeof doc.detachEvent
      (ev, fn, el = win)-> el.detachEvent "on#{ev}", fn
    else
      ->

  ###
    @brief Get key name from code

    @param {Number} code The key code
    @return {String} The name of the key
  ###
  @getKey = (code)->
    keyName[code]

  keyName =
    32: "space"
    13: "enter"
    9: "tab"
    8: "backspace"
    16: "shift"
    17: "ctrl"
    18: "alt"
    20: "capsLock"
    144: "numLock"
    145: "scrollLock"
    37: "left"
    38: "up"
    39: "right"
    40: "down"
    33: "pageUp"
    34: "pageDown"
    36: "home"
    35: "end"
    45: "insert"
    46: "delete"
    27: "escape"
    19: "pause"
