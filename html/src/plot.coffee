{canvas} = require "node"
{getInner} = require "dom"
{round, min, max} = Math

class @Plotter
  lineWidth: 1
  lineCap: "square"
  lineJoin: "bevel"
  lineStyle: "rgba(0,0,0,1.0)"
  fillStyle: "rgba(0,0,0,0.5)"
  aspect: 2
  points: null
  getter: (v)-> v
  
  constructor: (opts = {})->
    @[k] = v for k, v of opts

  $insert: (node)=>
    @$canvas = node.elm
    do @$redraw
  
  $remove: (node, done)=>
    @$canvas = null
    do done
  
  render: ->
    canvas
      class:
        card: on
      hook:
        insert: @$insert
        remove: @$remove

  redraw: ->
    do @$redraw

  $redraw: ->
    {$canvas, points, getter} = @
    return unless $canvas?

    {w:$canvas.width} = getInner $canvas.parentNode
    $canvas.height = if @aspect? then round $canvas.width / @aspect else $canvas.width

    {width, height} = $canvas

    ctx = $canvas.getContext "2d"
    ctx.clearRect 0, 0, width, height

    return unless points?.length

    [first, ..., last] = points

    [first_x, ...] = [min_x, min_y] = [max_x, max_y] = getter first
    [last_x, ...] = getter last

    for point in points[1...]
      [x, y] = getter point
      min_x = min min_x, x
      max_x = max max_x, x
      min_y = min min_y, y
      max_y = max max_y, y

    [min_x, max_x] = @xrange min_x, max_x, points.length if @xrange?
    [min_y, max_y] = @yrange min_y, max_y, points.length if @yrange?

    scale_x = width / (max_x - min_x)
    scale_y = height / (min_y - max_y)
    offset_x = round -min_x * scale_x
    offset_y = round -min_y * scale_y + height

    do ctx.save
    #ctx.setTransform scale_x, 0, 0, scale_y, offset_x, offset_y
    # we can't use transform because line width will be transformed also
    # instead we must convert coords itself
    conv = ([x, y])-> [(round x * scale_x) + offset_x, (round y * scale_y) + offset_y]
    
    ctx.lineWidth = @lineWidth
    ctx.lineCap = @lineCap
    ctx.lineJoin = @lineJoin
    ctx.strokeStyle = @lineStyle
    ctx.fillStyle = @fillStyle

    do ctx.beginPath
    
    ctx.moveTo.apply ctx, conv getter first
    ctx.lineTo.apply ctx, conv getter point for point in points[1...]

    do ctx.stroke

    ctx.lineTo.apply ctx, conv [last_x, min_y]
    ctx.lineTo.apply ctx, conv [first_x, min_y]
    do ctx.closePath
    
    do ctx.fill
    
    do ctx.restore
