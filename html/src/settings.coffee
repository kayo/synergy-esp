{put} = require "req"
{saveAs} = require "./FileSaver"
{parse, stringify} = JSON

#
# Controller
#

@Ctrl = (Base)->
  class Settings extends Base
    constructor: ->
      super
      do @switch_language
      @settings_filename = "settings.json"
      @settings_formatted = no

    param_loaded: ->
      @$param_group "settings", [
        "dev_name"
        "dev_desc"
        "httpd_username"
        "httpd_password"
      ]
      do @load_settings if @view_name is "settings"
      super

    show_settings: ->
      @load_settings?()

    set_language: (language)->
      return if language is @language
      @switch_language language
      do @$render

    set_httpd_show_password: (@httpd_show_password)->
      do @$render

    set_settings_filename: (@settings_filename)->
      do @$render

    set_settings_formatted: (@settings_formatted)->
      do @$render

    download_settings: ->
      put "/param", params = (name for name, {flag} of @params when "persist" in flag), (err, res)=>
        values = {}
        values[name] = res[index] for name, index in params
        saveAs (new Blob [if @settings_formatted
          stringify values, null, "  "
        else
          stringify values], type: "application/json"), @settings_filename

    attach_settings: ([file])->
      reader = new FileReader
      reader.onload = ({target:{result}})=>
        try
          values = parse result
        catch err
          return
        if "object" is typeof values
          @attached_settings = values
      reader.readAsText file

    upload_settings: ->
      put "/param", @attached_settings, (err, res)=>
        unless err
          @$param_get (name for name of @attached_settings), (err)=>
            do @$render

#
# View
#

{
  Row
  Tabs
  Tab
  Group
  Entry
  Select
  Check
  Button
  File
} = require "form"

{
  a
} = require "node"

{
  commit_label
  revert_label
  reset_label
  username_label
  password_label
  show_password_label
} = require "common"

dev_options_label =
  en: "Basic"
  ru: "Базовые"

dev_name_label =
  en: "Device name"
  ru: "Название устройства"

dev_desc_label =
  en: "Device description"
  ru: "Описание устройства"

ui_options_label =
  en: "Interface"
  ru: "Интерфейс"

language_label =
  en: "Language"
  ru: "Язык"

httpd_options_label =
  en: "HTTP-server"
  ru: "Сервер HTTP"

httpd_security_label =
  en: "Use SSL-protocol (HTTPS-server)"
  ru: "Использовать протокол SSL (сервер HTTPS)"

settings_save_restore_label =
  en: "Save and Restore"
  ru: "Сохранение и восстановление"

settings_filename_label =
  en: "Filename"
  ru: "Имя файла"

settings_formatted_label =
  en: "Save Formatted"
  ru: "Сохранять форматировано"

settings_save_label =
  en: "Save settings"
  ru: "Сохранить настройки"

settings_attach_label =
  en: "Attach"
  ru: "Присоединить"

settings_restore_label =
  en: "Restore settings"
  ru: "Восстановить настройки"

Settings = (opts)->
  {language} = opts
  
  Tabs
    name: "settings"
    style: "error"
    Tab
      label: dev_options_label[language]
      Entry
        label: dev_name_label[language]
        self: opts
        change: opts.set_dev_name
        value: opts.dev_name
        style: switch
          when opts.dev_name_error then "error"
          when opts._dev_name? then "success"
        tooltip: opts.dev_name_error?[language]
        disabled: not opts.dev_name?
        maxlength: opts.params?.dev_name?.size
      Entry
        label: dev_desc_label[language]
        multiline: yes
        rows: 4
        self: opts
        change: opts.set_dev_desc
        value: opts.dev_desc
        style: switch
          when opts.dev_desc_error then "error"
          when opts._dev_desc? then "success"
        tooltip: opts.dev_desc_error?[language]
        disabled: not opts.dev_desc?
        maxlength: opts.params?.dev_desc?.size
    Tab
      label: httpd_options_label[language]
      Entry
        label: username_label[language]
        self: opts
        change: opts.set_httpd_username
        value: opts.httpd_username
        style: switch
          when opts.httpd_username_error then "error"
          when opts._httpd_username? then "success"
        tooltip: opts.httpd_username_error?[language]
        disabled: not opts.httpd_username?
        maxlength: opts.params?.httpd_username?.size
      Entry
        label: password_label[language]
        self: opts
        change: opts.set_httpd_password
        value: opts.httpd_password
        password: not opts.httpd_show_password
        style: switch
          when opts.httpd_password_error then "error"
          when opts._httpd_password? then "success"
        tooltip: opts.httpd_password_error?[language]
        disabled: not opts.httpd_password?
        maxlength: opts.params?.httpd_password?.size
      Check
        label: show_password_label[language]
        value: opts.httpd_show_password
        self: opts
        change: opts.set_httpd_show_password
        disabled: not opts.httpd_password?
      Check
        label: httpd_security_label[language]
        value: opts.httpd_set_security
        checked: "on"
        unchecked: "off"
        self: opts
        change: opts.set_httpd_set_security
        disabled: not opts.httpd_set_security?
    Tab
      label: ui_options_label[language]
      Select
        id: "language"
        label: language_label[language]
        self: opts
        change: opts.set_language
        options: ({value, label} for value, {title: label} of opts.languages)
        value: opts.language
    Tab
      label: settings_save_restore_label[language]
      Entry
        label: settings_filename_label[language]
        self: opts
        change: opts.set_settings_filename
        value: opts.settings_filename
        disabled: not opts.params?
      Check
        label: settings_formatted_label[language]
        value: opts.settings_formatted
        self: opts
        change: opts.set_settings_formatted
        disabled: not opts.params?
      Button
        label: settings_save_label[language]
        self: opts
        click: opts.download_settings
        disabled: not opts.params?
      Group null,
        File
          label: settings_attach_label[language]
          self: opts
          attach: opts.attach_settings
          disabled: not opts.params?
        Button
          label: settings_restore_label[language]
          self: opts
          click: opts.upload_settings
          disabled: not opts.params? or not opts.attached_settings?
    Group
      controls: yes
      Button
        style: "primary"
        label: commit_label[language]
        disabled: not opts.settings_changed or not opts.settings_valid
        self: opts
        click: opts.commit_settings
      Button
        style: "warning"
        label: revert_label[language]
        disabled: not opts.settings_changed
        self: opts
        click: opts.revert_settings
      Button
        style: "error"
        label: reset_label[language]
        disabled: not opts.settings_custom
        self: opts
        click: opts.reset_settings

init_title =
  en: "Initializaton"
  ru: "Инициализация"

@tags = [
  (opts)->
    a
      class:
        brand: on
      attrs:
        href: "#"
      if opts.params? and opts.dev_name?
        opts.dev_name
      else
        init_title[opts.language]
,
  (opts)->
    {params, language} = opts
    if params?
      a
        class:
          label: on
          warning: on
        "#{params?.fw_device?.def or ""} v#{params?.fw_version?.def or ""}"
]

@views = [
  name: "settings"
  view: Settings
  title:
    en: "Settings"
    ru: "Настройки"
]
