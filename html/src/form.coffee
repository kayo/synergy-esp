{
  div
  span
  p
  a
  legend
  label
  input
  textarea
  select
  option
  button
} = require "node"

empty = {}
dummy = ->
unique = do ->
  n = 0
  -> n++

styles =
  success: 1
  warning: 1
  error: 1
  pseudo: 1

Control = (opts, elem)->
  if opts.nowrap
    elem
  else
    p
      class:
        control: yes
        csuccess: opts.style is "success"
        cwarning: opts.style is "warning"
        cerror: opts.style is "error"
      attrs:
        "data-tooltip": opts.tooltip
      if opts.label
        label
          attrs:
            for: opts.id
          opts.label
      elem

@Entry = (opts)->
  change = if opts.change?
    ({target:{value}})-> opts.change.call @, value
  else
    dummy
  
  Control opts,
    (if opts.rows then textarea else input)
      attrs:
        id: opts.id
        type: opts.password and "password" or "text" unless opts.rows
        rows: opts.rows if (opts.rows|0) > 0
        value: opts.value
        disabled: opts.disabled
        required: opts.required
        placeholder: opts.placeholder
        readonly: opts.readonly
        maxlength: opts.maxlength
      props:
        value: opts.value
      self: opts.self
      on:
        input: change
        change: change

@Select = (opts)->
  change = if opts.change?
    ({target:{value}})-> opts.change.call @, value
  else
    dummy
  Control opts,
    select
      attrs:
        disabled: opts.disabled
        required: opts.required
      self: opts.self
      on:
        change: change
      for opt in opts.options
        option
          attrs:
            value: opt.value
            selected: opt.value is opts.value
          opt.label

fnChecked = (values)->
  if Array.isArray values
    [
      (value)-> -1 isnt values.indexOf value # is checked
    ,
      (value)-> # trigger checked
        index = values.indexOf value
        if index is -1
          values = [values..., value]
        else
          values = [values[...index]..., values[index+1...]...]
        values
    ]
  else if "object" is typeof values
    [
      (value)-> values[value] # is checked
    ,
      (value)-> # trigger checked
        _values = values
        values = {}
        values[key] = val for key, val of _values
        values[value] = not values[value]
        values
    ]
  else
    [ dummy, dummy ]

@Check = (opts)->
  if opts.options
    [check, trig] = fnChecked opts.value
    
    change = if opts.change?
      (value)-> opts.change.call @, trig value
    else
      dummy
    
    Control opts,
      for opt in opts.options
        label null,
          input
            attrs:
              type: "checkbox"
              checked: check opt.value
              disabled: opts.disabled
              required: opts.required
            self: opts.self
            on:
              #click: [change, opt.value]
              change: [change, opt.value]
          span
            class:
              checkable: on
            opt.label
  else
    change = if opts.change?
      ({target})->
        opts.change.call @, if opts.checked?
          if target.checked
            opts.checked
          else
            opts.unchecked
        else
          target.checked
    else
      dummy

    checked = if opts.checked?
      opts.value is opts.checked
    else
      not not opts.value

    div
      class:
        controls: on
      label null,
        input
          attrs:
            type: "checkbox"
            checked: checked
            disabled: opts.disabled
            required: opts.required
          props:
            checked: checked
          self: opts.self
          on:
            change: change
        span
          class:
            checkable: on
          opts.label

###
@Toggle = (opts)->
  if opts.options
    [check, trig] = fnChecked opts.value
    
    change = if opts.change?
      (value)-> opts.change.call @, trig value
    else
      dummy
    
    Control opts,
      for opt in opts.options
        style = opt.style or opts.style
        button
          class:
            "#{style}": style of styles
            active: check opt.value
          attrs:
            disabled: opts.disabled
          self: opts.self
          on:
            click: [change, opt.value]
          opt.label
  else
    change = if opts.change?
      -> opts.change.call @, not opts.value
    else
      dummy
    div
      class:
        controls: on
      button
        class:
          "#{opts.style}": opts.style of styles
          active: check opt.value
        attrs:
          disabled: opts.disabled
        self: opts.self
        on:
          click: change
        opts.label
###

@Radio = (opts)->
  name = opts?.name or "radio-#{do unique}"
  change = opts?.change or dummy
  
  Control opts,
    for opt in opts.options
      label null,
        input
          attrs:
            name: name
            type: "radio"
            checked: opts.value is opt.value
          self: opts.self
          on:
            change: [change, opt.value]
        span
          class:
            checkable: on
          opt.label

@Button = Button = (opts)->
  button
    class:
      "#{opts.style}": opts.style of styles
      active: opts.active
    attrs:
      disabled: opts.disabled
    self: opts.self
    on:
      click: opts.click or dummy
      drop: opts.drop
      dragenter: opts.dragenter
      dragover: opts.dragover
      dragleave: opts.dragleave
    opts.label

@File = (opts)->
  change = if opts.attach
    ({target:{files}})->
      opts.attach.call @, files
  else
    dummy
  
  opts.drop = if opts.attach
    ({dataTransfer:{files}})->
      opts.attach files
  else
    dummy
  
  opts.click = ({target})->
    do target.firstChild?.click
  
  opts.label = [
    input
      attrs:
        type: "file"
        multiple: not not opts.multiple
      style:
        display: "none"
      self: opts.self
      on:
        change: change
    opts.label
  ]
  
  Button opts

@Form = (opts, fields...)->
  opts ?= empty
  div
    class:
      "form-aligned": opts.aligned
    fields

@Group = (opts, controls...)->
  opts ?= empty
  p
    class:
      #card: yes
      controls: opts.controls
    if opts?.label
      legend null,
        opts.label
    controls

@Row = Row = (opts, content...)->
  div
    class:
      row: yes
    content

@Alert = (opts)->
  div
    class:
      card: on
      alert: on
      success: "success" is opts.style
      warning: "warning" is opts.style
      error: "error" is opts.style
    p null,
      opts.message
    if opts.close?
      a
        class:
          close: on
        self: opts.self
        on:
          click: opts.close
        "×"

class Tab
  constructor: (@label, @content)->

@Tab = (opts, content...)->
  new Tab opts.label, content

counts =
  2: "two"
  3: "three"
  4: "four"

@Tabs = (opts, tabs...)->
  name = opts?.name or "tabs-#{do unique}"
  style = opts?.style

  labels = []
  contents = []
  
  for element in tabs when element instanceof Tab
    id = "#{name}-tab-#{contents.length}"
    labels.push input
      attrs:
        id: id
        type: "radio"
        name: name
        checked: contents.length is 0
    labels.push label
      attrs:
        for: id
      class:
        button: yes
        toggle: yes
        "#{style}": style of styles
      element.label
    contents.push div
      class:
        tab: on
      element.content
  
  div
    class:
      tabs: on
      "#{counts[contents.length]}": on
    labels
    Row null,
      contents
    element for element in tabs when element not instanceof Tab
