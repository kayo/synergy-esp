var docSelection = document.selection,
    inputSelection = docSelection && docSelection.createRange ? {
  has: function(elm) {
    return !!elm.createTextRange;
  },
  get: function(elm) {
    var start, end, range = docSelection.createRange();

    if (range && range.parentElement() === elm) {
      var len = elm.value.length;
      var normalizedValue = elm.value.replace(/\r\n/g, "\n");

      /* Create a working TextRange that lives only in the input */
      var textInputRange = elm.createTextRange();
      textInputRange.moveToBookmark(range.getBookmark());

      /*
       * Check if the start and end of the selection are at the very end
       * of the input, since moveStart/moveEnd doesn't return what we want
       * in those cases
       */
      var endRange = elm.createTextRange();
      endRange.collapse(false);

      if (textInputRange.compareEndPoints("StartToEnd", endRange) > -1) {
        start = end = len;
      } else {
        start = -textInputRange.moveStart("character", -len);
        start += normalizedValue.slice(0, start).split("\n").length - 1;

        if (textInputRange.compareEndPoints("EndToEnd", endRange) > -1) {
          end = len;
        } else {
          end = -textInputRange.moveEnd("character", -len);
          end += normalizedValue.slice(0, end).split("\n").length - 1;
        }
      }
    }

    return [start, end];
  },
  set: function(elm, selection) {
    var start = selection[0],
        end = selection[1],
        normalizedValue = elm.value.replace(/\r\n/g, "\n");

    start -= normalizedValue.slice(0, start).split("\n").length - 1;
    end -= normalizedValue.slice(0, end).split("\n").length - 1;

    var range = elm.createTextRange();
    range.collapse(true);
    range.moveEnd('character', end);
    range.moveStart('character', start);
    range.select();
  }
} : {
  has: function(elm) {
    return !!elm.setSelectionRange;
  },
  get: function(elm) {
    return [elm.selectionStart, elm.selectionEnd];
  },
  set: function(elm, selection) {
    elm.setSelectionRange.apply(elm, selection);
  }
};

function updateProps(oldVnode, vnode) {
  var key, cur, old, elm = vnode.elm,
      oldProps = oldVnode.data.props || {}, props = vnode.data.props || {},
      valueDiffer = props.value != null
                 && props.value != elm.value,
      selectionStart = props.selectionStart,
      selectionEnd = props.selectionEnd,
      selection = document.activeElement === elm
               && inputSelection.has(elm)
               && (valueDiffer
                 || props.value != oldProps.value
                 || typeof selectionStart === "number"
                 || typeof selectionEnd === "number")
                ? inputSelection.get(elm) : null;
  if (selection) {
    if (typeof selectionStart === "number")
      selection[0] = selectionStart;
    if (typeof selectionEnd === "number")
      selection[1] = selectionEnd;
  }
  for (key in oldProps) {
    if (!props[key]) {
      delete elm[key];
    }
  }
  for (key in props) {
    cur = props[key];
    old = oldProps[key];
    if ((old !== cur || key === "value" && valueDiffer) &&
        key !== "selectionStart" && key !== "selectionEnd") {
      elm[key] = cur;
    }
  }
  /* restore excursion */
  if (selection) {
    inputSelection.set(elm, selection);
    elm.focus();
  }
}

module.exports = {create: updateProps, update: updateProps};
