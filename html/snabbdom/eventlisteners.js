var is = require('snabbdom/is');

function arrInvoker(arr) {
  return function() {
    // Special case when length is two, for performance
    arr.length === 2 ? arr[0].call(arr.self, arr[1]) : arr[0].apply(arr.self, arr.slice(1));
  };
}

function fnInvoker(o) {
  return function(ev) { o.fn.call(o.self, ev); };
}

function updateEventListeners(oldVnode, vnode) {
  var name, cur, old, elm = vnode.elm,
      self = vnode.data.self,
      oldOn = oldVnode.data.on || {}, on = vnode.data.on;
  if (!on) return;
  for (name in on) {
    cur = on[name];
    old = oldOn[name];
    if (cur === undefined) {
      continue;
    }
    if (old === undefined) {
      if (is.array(cur)) {
        elm.addEventListener(name, arrInvoker(cur));
      } else {
        cur = {fn: cur};
        on[name] = cur;
        elm.addEventListener(name, fnInvoker(cur));
      }
      cur.self = self;
    } else {
      if (is.array(old)) {
        // Deliberately modify old array since it's captured in closure created with `arrInvoker`
        old.length = cur.length;
        for (var i = 0; i < old.length; ++i) old[i] = cur[i];
        old.self = self;
        on[name] = old;
      } else {
        old.fn = cur;
        old.self = self;
        on[name] = old;
      }
    }
  }
}

module.exports = {create: updateEventListeners, update: updateEventListeners};
