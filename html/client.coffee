require "es5-shim"
require "es5-shim/es5-sham"
require "./shim/xhr.activex"
require "./shim/xhr.binary"

unless VDOM_RENDER is "simple"
  require "animation-frame"
  .shim frameRate: 60

#require "eventsource-polyfill"

require "./client.scss"

{
  document
  requestAnimationFrame
} = window

{init} = require "snabbdom"
patch = init [ # Init patch function with choosen modules
  require "snabbdom/modules/class" # makes it easy to toggle classes
  #require "snabbdom/modules/props" # for setting properties on DOM elements
  require "./snabbdom/props"
  require "snabbdom/modules/attributes" # for setting attributes on DOM elements
  require "snabbdom/modules/style" # handles styling on elements with support for animations
  #require "snabbdom/modules/eventlisteners" # attaches event listeners
  require "./snabbdom/eventlisteners"
]

{Main: Base, Root} = require "./src/main"

SimpleRender = (Base)->
  class SimpleRender extends Base
    $render: -> do @$patch

DirectRender = (Base)->
  class DirectRender extends Base
    $renderFrame: => do @$patch
    $render: -> requestAnimationFrame @$renderFrame

SmartRender = (Base)->
  class SmartRender extends Base
    constructor: ->
      super
      @_pendRender = null
      @_needRender = no

    $renderFrame: =>
      @_pendRender = null
      do @$patch
      if @_needRender
        @_needRender = no
        do @$forceRender

    $forceRender: ->
      @_pendRender = requestAnimationFrame @$renderFrame

    $render: ->
      return if @_needRender
      if @_pendRender?
        @_needRender = yes
      else
        do @$forceRender

class Main extends (switch VDOM_RENDER
  when "smart" then SmartRender
  when "direct" then DirectRender
  else SimpleRender) Base

  constructor: (@Root)->
    super

  $tree: ->
    _domTree = @_domTree
    @_domTree = @Root @
    _domTree

  $mount: (@_rootNode)->
    do @$tree
    patch @_rootNode, @_domTree
    super

  $patch: ->
    return unless @_rootNode?
    _domTree = do @$tree
    patch _domTree, @_domTree
    super

boot = ->
  new Main Root
  .$mount document.body

document.addEventListener (switch TARGET
  when "cordova" then "deviceready"
  else "DOMContentLoaded"), boot, no
