(function(){
  var XHR = XMLHttpRequest,
  XHRProto;
  if(XHR &&
     (XHRProto = XHR.prototype) &&
     !XHRProto.sendAsBinary &&
     typeof Uint8Array != 'undefined'){
    XHRProto.sendAsBinary = function(data) {
      var buffer = new ArrayBuffer(data.length),
      bytes = new Uint8Array(buffer),
      i = 0;
      for(; i < data.length; bytes[i] = data[i++] & 0xff);
      this.send(buffer);
    };
  }
}).apply(this);
