(function(){
  if (!this.XMLHttpRequest && this.ActiveXObject) {
    var variants = [
      'Msxml2.XMLHTTP.6.0',
      'Msxml2.XMLHTTP.5.0',
      'Msxml2.XMLHTTP.4.0',
      'Msxml2.XMLHTTP.3.0',
      'Msxml2.XMLHTTP',
      'Microsoft.XMLHTTP'
    ], variant, i;

    for(i = 0; i < variants.length; i++){
      if(new ActiveXObject(variants[i])){
        variant = variants[i];
        this.XMLHttpRequest = function(){
          return new ActiveXObject(variant);
        };
        break;
      }
    }
  }
}).apply(window);
