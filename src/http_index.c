#include "http_conn.h"

static marten_define_writer(get_root_response) {
  http_conn_t *conn = marten_containerof(writer, http_conn_t, response);
  marten_begin(writer);
  marten_status_cstr_write(writer, "HTTP/1.1", 200, "OK");
  marten_conn_header_write(writer, conn->state);
  marten_header_cstr_write(writer, "Content-Type", "text/html");
  marten_header_cstr_write(writer, "Content-Encoding", "gzip");

  blob_def(index_html_gz);
  {
    estr_t str;
    estr_extern_init(&str, conn->buffer);
    
    estr_putsc(&str, "Content-Length: ");
    estr_putud(&str, blob_len(index_html_gz));
    estr_putsc(&str, "\r\n\r\n");
    
    marten_estr_write(writer, &str);
  }
  
  marten_raw_write(writer, blob_ptr(index_html_gz), blob_len(index_html_gz));
  marten_end(writer);
}

marten_define_handler(get_root_handler) {
  /* because we use the same handler to handle request
     and query parameters we need correctly get container */
  http_conn_t *conn = marten_is_ent(flags, marten_query)
    ? marten_containerof(state, http_conn_t, query)
    : marten_containerof(state, http_conn_t, route);
  
  if (marten_is_raw(flags, marten_query)) {
    /* you can parse query variables using same handler */
    marten_query_parse(&conn->query, get_root_handler, flags, ptr, len);
  }
  
  if (marten_has(flags, marten_done)) { /* entity is done */
    if (marten_is(flags, marten_head) &&
        !marten_has(flags, marten_field | marten_value)) {
      debug("headers is done");
      
      marten_respond(&conn->response, get_root_response);
      return 1;
    }
  }
  
  return 0;
}
