#include "http_conn.h"
#include "user_interface.h"

#include "elastr.h"

enum {
  scan_invalid_state = 1,
  scan_failed_action,
};

static struct {
  struct {
    char *data;
    uint16_t len;
    uint8_t usage;
  } response;
  int8_t in_progress;
} scan = {
  {
    NULL,
    0,
    0,
  },
  0
};

static marten_define_writer(get_scan_response) {
  http_conn_t *conn = marten_containerof(writer, http_conn_t, response);
  
  marten_begin(writer);
  marten_status_cstr_write(writer, "HTTP/1.1", 200, "OK");
  marten_conn_header_write(writer, conn->state);
#if HTTPD_USE_YAML
  marten_header_cstr_write(writer, "Content-Type", "application/yaml");
#else
  marten_header_cstr_write(writer, "Content-Type", "application/json");
#endif
  
  {
    estr_t str;
    estr_extern_init(&str, conn->buffer);
    
    estr_putsc(&str, "Content-Length: ");
    estr_putud(&str, scan.response.len);
    estr_putsc(&str, "\r\n\r\n");
    
    marten_estr_write(writer, &str);
  }
  
  marten_raw_write(writer, scan.response.data, scan.response.len);
  marten_end(writer);

  if ((--scan.response.usage) == 0) {
    debug("free scan data");
    port_free(scan.response.data);
    
    scan.response.len = 0;
    scan.response.data = NULL;
  }
}

static marten_define_writer(get_scan_failed) {
  http_conn_t *conn = marten_containerof(writer, http_conn_t, response);
  
  marten_begin(writer);
  marten_status_cstr_write(writer, "HTTP/1.1", 409, "Conflict");
  marten_conn_header_write(writer, conn->state);
  marten_header_cstr_write(writer, "Content-Type", "application/yaml");
  
  if (conn->scan.state == scan_invalid_state) {
    marten_header_cstr_write(writer, "Content-Length", "29");
  } else if (conn->scan.state == scan_failed_action) {
    marten_header_cstr_write(writer, "Content-Length", "22");
  } else {
    marten_header_cstr_write(writer, "Content-Length", "21");
  }
  
  marten_feed_write(writer);
  
  if (conn->scan.state == scan_invalid_state) {
    marten_cstr_write(writer, "error: Invalid operation mode");
  } else if (conn->scan.state == scan_failed_action) {
    marten_cstr_write(writer, "error: Scanning failed");
  } else {
    marten_cstr_write(writer, "error: Scanning error");
  }
  
  marten_end(writer);
}

static void scan_respond_all(marten_writer_t *response) {
  /* respond to all scan requests */
  http_conn_t *conn = NULL;
  
  for (; (conn = marten_conn_next(server->pool, conn)) != NULL; ) {
    if (conn->route == marten_router_route(&http_router, "GET", "/scan")) {
      scan.response.usage ++;
      marten_respond(&conn->response, response);
    }
  }
}

static void on_scan_done(void *arg, STATUS status) {
  if (status == OK && arg != NULL) {
    estr_t res;
    struct bss_info *bss_link;
    size_t i;
    
    estr_heap_init(&res, NULL);

#if HTTPD_USE_YAML
    for (bss_link = arg; bss_link != NULL;
         bss_link = bss_link->next.stqe_next) {
      estr_putsc(&res, "- bssid: ");
      estr_putux(&res, bss_link->bssid[0]);
      
      for (i = 1; i < sizeof(bss_link->bssid)/sizeof(bss_link->bssid[0]); i++) {
        estr_putc(&res, ':');
        estr_putux(&res, bss_link->bssid[i]);
      }
      
      estr_putsc(&res, "\r\n  ssid: ");
      estr_putsr(&res, bss_link->ssid);
      
      estr_putsc(&res, "\r\n  channel: ");
      estr_putud(&res, bss_link->channel);
      
      estr_putsc(&res, "\r\n  rssi: ");
      estr_putsd(&res, bss_link->rssi);
      
      estr_putsc(&res, "\r\n  auth: ");
      if (bss_link->authmode == AUTH_OPEN) {
        estr_putsc(&res, "open");
      } else if (bss_link->authmode == AUTH_WEP) {
        estr_putsc(&res, "wep");
      } else if (bss_link->authmode == AUTH_WPA_PSK) {
        estr_putsc(&res, "wpa");
      } else if (bss_link->authmode == AUTH_WPA2_PSK) {
        estr_putsc(&res, "wpa2");
      } else if (bss_link->authmode == AUTH_WPA_WPA2_PSK) {
        estr_putsc(&res, "wpa+wpa2");
      }
      
      if (bss_link->is_hidden) {
        estr_putsc(&res, "\r\n  hidden: y");
      }
      
      estr_putsc(&res, "\r\n  fqoff: ");
      estr_putsd(&res, bss_link->freq_offset);
      
      estr_putsc(&res, "\r\n");
    }
#else
    estr_putc(&res, '[');
    for (bss_link = arg; bss_link != NULL;
         bss_link = bss_link->next.stqe_next) {
      if (*estr_ptr(&res, -1) != '[')
        estr_putc(&res, ',');
      estr_putsc(&res, "{\"bssid\":\"");
      estr_putux(&res, bss_link->bssid[0]);
      
      for (i = 1; i < sizeof(bss_link->bssid)/sizeof(bss_link->bssid[0]); i++) {
        estr_putc(&res, ':');
        estr_putux(&res, bss_link->bssid[i]);
      }
      
      estr_putsc(&res, "\",\"ssid\":\"");
      estr_putsq(&res, bss_link->ssid);
      
      estr_putsc(&res, "\",\"channel\":");
      estr_putud(&res, bss_link->channel);
      
      estr_putsc(&res, ",\"rssi\":");
      estr_putsd(&res, bss_link->rssi);
      
      estr_putsc(&res, ",\"auth\":\"");
      if (bss_link->authmode == AUTH_OPEN) {
        estr_putsc(&res, "open");
      } else if (bss_link->authmode == AUTH_WEP) {
        estr_putsc(&res, "wep");
      } else if (bss_link->authmode == AUTH_WPA_PSK) {
        estr_putsc(&res, "wpa");
      } else if (bss_link->authmode == AUTH_WPA2_PSK) {
        estr_putsc(&res, "wpa2");
      } else if (bss_link->authmode == AUTH_WPA_WPA2_PSK) {
        estr_putsc(&res, "wpa+wpa2");
      }
      estr_putc(&res, '"');
      
      if (bss_link->is_hidden) {
        estr_putsc(&res, ",\"hidden\":true");
      }
      
      estr_putsc(&res, ",\"fqoff\":");
      estr_putsd(&res, bss_link->freq_offset);
      
      estr_putc(&res, '}');
    }
    estr_putc(&res, ']');
#endif
    
    scan.response.len = estr_len(&res);
    scan.response.data = estr_str(&res);
    
    scan_respond_all(get_scan_response);
  } else {
    scan_respond_all(get_scan_failed);
  }
  
  scan.in_progress = 0;
}

marten_define_handler(get_scan_handler) {
  (void)flags;
  (void)ptr;
  (void)len;
  
  http_conn_t *conn = marten_containerof(state, http_conn_t, route);
  
  if (STATION_MODE & wifi_get_opmode()) {
    if (scan.response.data != NULL) {
      scan.response.usage ++;
      marten_respond(&conn->response, get_scan_response);
    }
    if (!scan.in_progress &&
        !wifi_station_scan(NULL, on_scan_done)) {
      conn->scan.state = scan_failed_action;
      marten_respond(&conn->response, get_scan_failed);
    }
  } else {
    conn->scan.state = scan_invalid_state;
    marten_respond(&conn->response, get_scan_failed);
  }
  
  return 1;
}
