#include "user_config.h"
#include "user_interface.h"
#include "http_server.h"
#include "http_conn.h"

#ifdef HTTPD_AUTH
extern char httpd_username[32];
extern char httpd_password[32];
#endif
#ifdef HTTPD_SECURE
extern uint8_t httpd_security;
#endif

const char *ent_name(marten_flags_t flags) {
  switch (flags & marten_entity) {
  case marten_method: return "method";
  case marten_path: return "path";
  case marten_query: return "query";
  case marten_proto: return "proto";
  case marten_head: return "head";
  case marten_body: return "body";
  case marten_content: return "content";
  }
  return "unknown";
}

const char *ent_part(marten_flags_t flags) {
  switch (flags & marten_entry) {
  case marten_field: return " field";
  case marten_value: return " value";
  }
  return "";
}

marten_define_pool_static(http_conn_pool, http_conn_t, HTTPD_CONNPOOL);

http_server_t http_server = {
  .pool = marten_provide_pool_static(http_conn_pool)
};

marten_define_router(http_router, HTTPD_ROUTES);

static marten_define_writer(notfound_response) {
  http_conn_t *conn = marten_containerof(writer, http_conn_t, response);
  marten_begin(writer);
  marten_status_cstr_write(writer, "HTTP/1.1", 404, "Not Found");
  marten_conn_header_write(writer, conn->state);
  marten_header_cstr_write(writer, "Content-Type", "text/plain");
  marten_header_cstr_write(writer, "Content-Length", "18");
  marten_feed_write(writer);
  marten_cstr_write(writer, "Resource Not Found");
  marten_end(writer);
}

static marten_define_handler(on_conn_data) {
  http_conn_t *conn = marten_is_ent(flags, marten_head) ?
    marten_containerof(state, http_conn_t, header) :
    marten_containerof(state, http_conn_t, request);
  
  rawlog("conn %p: parse " flags_fmt, ptr, len, conn, show_flags(flags));
  
  if (marten_is(flags, marten_method) &&
      marten_has(flags, marten_init)) {
    marten_reset(conn->state,
                 marten_conn_keep |
                 marten_conn_part |
                 marten_conn_data);
  }
  
  if (marten_is_raw(flags, marten_head)) {
    marten_head_parse(&conn->header, on_conn_data, flags, ptr, len);
  }
  
  marten_field_pick(&conn->key_hash, flags, ptr, len);
  
  if ((marten_is(flags, marten_head) || marten_is(flags, marten_proto)) &&
      marten_has(flags, marten_value)) {
    if (conn->key_hash == marten_hash_cstr("transfer-encoding") ||
        conn->key_hash == marten_hash_cstr("connection")) {
      marten_value_pick(&conn->hash, flags, ptr, len);
    } else if (conn->key_hash == marten_hash_cstr("keep-alive")) {
    }
    
    if (marten_has(flags, marten_done)) {
      if (conn->key_hash == marten_hash_cstr("transfer-encoding")) {
        if (conn->hash == marten_hash_cstr("chunked")) {
          marten_set(conn->state, marten_conn_part);
        }
      } else if (conn->hash == marten_hash_cstr("connection")) {
        if (conn->hash == marten_hash_cstr("close")) {
          marten_reset(conn->state, marten_conn_keep);
        } else if (conn->hash == marten_hash_cstr("keep-alive")) {
          marten_set(conn->state, marten_conn_keep);
        }
      }
    }
  }
  
  int res = marten_router_handle(&http_router, &conn->route, &conn->hash, flags, ptr, len);
  
  if (res != 0) {
    if (conn->route == marten_route_none) {
      marten_respond(&conn->response, notfound_response);
    }
  }
  
  return res;
}

static err_t
on_recv(void *arg,
        struct tcp_pcb *pcb,
        struct pbuf *buf,
        err_t err) {
  http_conn_t *conn = arg;
  
  if (err != ERR_OK) {
    debug("recv error");
    goto close;
  }
  
  if (buf == NULL) {
    debug("empty recv buffer");
    goto close;
  }

  uint8_t *data;
  int size;
  
#ifdef HTTPD_SECURE
  if (httpd_security) {
    size = axl_ssl_read(conn->ssl, &data, pcb, buf);
#if _DEBUG_IF(debug)
    static char show_once = 0;
    
    if(!show_once && ssl_handshake_status(conn->ssl) == SSL_OK) {
      const char *common_name = ssl_get_cert_dn(conn->ssl, SSL_X509_CERT_COMMON_NAME);
      if (common_name) {
        debug("Common Name:\t\t\t%s\n", common_name);
      }
      
      //display_session_id(conn->ssl); 
      //display_cipher(conn->ssl);
      show_once = 1;
    }
#endif /* _DEBUG_IF(debug) */
  } else {
#endif
    data = buf->payload;
    size = buf->tot_len;
    tcp_recved(pcb, buf->tot_len);
#ifdef HTTPD_SECURE
  }
#endif
  
  debug("received");

  if (size > 0) {
    rawlog("conn %p: recv", data, size, conn);

    if (marten_has(conn->state, marten_conn_recv)) {
      debug("conn %p: resume parse", conn);
      
      if (0 != marten_request_parse(&conn->request, on_conn_data, marten_none, data, size)) {
        marten_reset(conn->state, marten_conn_recv);
      }
    }
  }
  
  pbuf_free(buf);
  
 done:
  return ERR_OK;

 close:
  /*
  tcp_arg(pcb, NULL);
  tcp_sent(pcb, NULL);
  tcp_recv(pcb, NULL);
  */
  tcp_close(pcb);
  
  marten_conn_done(server->pool, conn);
  
  goto done;
}

static err_t
on_sent(void *arg,
        struct tcp_pcb *pcb,
        u16_t len);

static void
trig_send(http_conn_t *conn) {
  size_t len = conn->send.len;
  
  if (len > tcp_sndbuf(conn->pcb)) {
    len = tcp_sndbuf(conn->pcb);
  }
  
  if (((uint32_t)conn->send.ptr >> 30) > 0) { /* check ptr is flash rom */
    if (len < 4) {
      const uint32_t *s = (void*)conn->send.ptr;
      uint32_t *d = (void*)(((uint32_t)conn->buffer + 4) & ~3);
      *d = *s;
      conn->send.ptr = (char*)d;
    } else {
      len &= ~3;
    }
  }
  
  if (len > 0) {
    rawlog("conn %p: send", conn->send.ptr, len, conn);

#ifdef HTTPD_SECURE
    if (httpd_security) {
      axl_ssl_write(conn->ssl, conn->send.ptr, len);
    } else {
#endif
      tcp_write(conn->pcb, conn->send.ptr, len, 0);
      //tcp_output(conn->pcb);
#ifdef HTTPD_SECURE
    }
#endif
    
    conn->send.ptr += len;
    conn->send.len -= len;
  }
}

static marten_define_handler(do_send) {
  (void)flags;
  
  http_conn_t *conn = marten_containerof(state, http_conn_t, response);
  
  conn->send.ptr = ptr;
  conn->send.len = len;
  
  trig_send(conn);
  
  return 0;
}

static void
do_init(http_conn_t *conn) {
  marten_set(conn->state, marten_conn_recv);
  
  marten_request_parse(&conn->request, on_conn_data, marten_init, NULL, 0);
}

static err_t
on_sent(void *arg,
        struct tcp_pcb *pcb,
        u16_t len) {
  (void)len;
  http_conn_t *conn = arg;
  
  debug("conn %p: on_sent", conn);
  
  /* continue send current chunk */
  if (conn->send.len > 0) {
    trig_send(conn);
    return ERR_OK;
  }
  
  debug("conn %p: resume write %u", conn, conn->response.stage);
  
  /* resume writer coroutine */
  if (0 == marten_resume_write(&conn->response)) {
    debug("conn %p: write resumed %u", conn, conn->response.stage);
    return ERR_OK;
  }
  
  debug("conn %p: write done", conn);
  
  if (marten_has(conn->state, marten_conn_keep)) {
    /* restart parser */
    debug("conn %p: keep", conn);
    
    do_init(conn);
  } else { /* close connection */
    tcp_close(pcb);
    marten_conn_done(server->pool, conn);
  }
  
  return ERR_OK;
}

static void
on_fail(void *arg,
       err_t err) {
  (void)err;
  
  http_conn_t *conn = arg;
  
  debug("conn: %p tcp error: %d", conn, err);
  
  /* free connection on error */
  marten_conn_done(server->pool, conn);
}

/*
static err_t
on_poll(void *arg, struct tcp_pcb *pcb) {
  (void)pcb;
  http_conn_t *conn = arg;
  
  debug("conn %p: poll state:%d", conn, pcb->state);
  
  return ERR_OK;
}
*/

static err_t
on_conn(void *arg,
        struct tcp_pcb *pcb,
        err_t err) {
  (void)arg;
  (void)err;
  //if (err != ERR_OK) {
  //  return err;
  //}
  
  assert(err == ERR_OK);
  
  tcp_setprio(pcb, TCP_PRIO_MIN);
  
  http_conn_t *conn = marten_conn_init(server->pool);

  if (conn == NULL) {
    error("too many connections");
    return ERR_MEM;
  }

  tcp_accepted(server->pcb);
  debug("accepted");
  
  conn->pcb = pcb;
  conn->response.write = do_send;
  
  do_init(conn);
  
  tcp_arg(pcb, conn);
  tcp_recv(pcb, on_recv);
  tcp_err(pcb, on_fail);
  //tcp_poll(pcb, on_poll, 0);
  tcp_sent(pcb, on_sent);

#ifdef HTTPD_SECURE
  int ssl_fd = axl_append(pcb);
  
  if(ssl_fd == -1) {
    error("Unable to add LWIP tcp -> clientfd mapping");
    return ERR_OK;
  }

  conn->ssl_ctx = ssl_ctx_new(
#ifdef HTTPD_DEBUG
                              SSL_DISPLAY_STATES | SSL_DISPLAY_BYTES |
#endif
                              SSL_CONNECT_IN_PARTS, 1);

  conn->ssl = ssl_server_new(conn->ssl_ctx, ssl_fd);
#endif
  
  marten_request_parse(&conn->request, on_conn_data, marten_init, NULL, 0);
  
  return ERR_OK;
}

void http_server_start(void) {
#ifdef HTTPD_SECURE
  axl_init(SECURE_NUMCONN);
#endif
  
  server->pcb = tcp_new();
  assert(server->pcb != NULL);
  tcp_bind(server->pcb, IP_ADDR_ANY,
#ifdef HTTPD_SECURE
           httpd_security ? 443 :
#endif
           80);
  server->pcb = tcp_listen_with_backlog(server->pcb, HTTPD_BACKLOG);
  assert(server->pcb != NULL);
  tcp_accept(server->pcb, on_conn);
  
  info("started");
  debug("sizeof(http_conn_t) == %u", sizeof(http_conn_t));
}

void http_server_stop(void) {
  tcp_close(server->pcb);
  
  info("stopped");
}
