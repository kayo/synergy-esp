#include "http_conn.h"

#define test_data ((uint8_t*)0x70000000)

#ifndef test_len
#  define test_len (1024 * 64)
#endif

#ifndef test_num
#  define test_num (1024 * 1024)
#endif

static marten_define_writer(get_test_response) {
  http_conn_t *conn = marten_containerof(writer, http_conn_t, response);
  marten_begin(writer);
  marten_status_cstr_write(writer, "HTTP/1.1", 200, "OK");
  marten_conn_header_write(writer, conn->state);
  marten_header_cstr_write(writer, "Content-Type", "application/octet-stream");
  marten_feed_write(writer);
  
  for (; conn->test.count--; ) {
    marten_raw_write(writer, test_data, test_len);
  }
  
  marten_end(writer);
}

marten_define_handler(get_test_handler) {
  (void)flags;
  (void)ptr;
  (void)len;
  
  http_conn_t *conn = marten_containerof(state, http_conn_t, route);

  conn->test.count = test_num;
  marten_respond(&conn->response, get_test_response);
  
  return 1;
}

static marten_define_writer(put_test_response) {
  http_conn_t *conn = marten_containerof(writer, http_conn_t, response);
  
  marten_begin(writer);
  marten_status_cstr_write(writer, "HTTP/1.1", 204, "No Content");
  marten_conn_header_write(writer, conn->state);
  marten_feed_write(writer);
  marten_end(writer);
}

static marten_define_handler(put_test_header) {
  http_conn_t *conn = marten_containerof(state, http_conn_t, header);
  
  marten_field_pick(&conn->key_hash, flags, ptr, len);

  if (marten_has(flags, marten_value) &&
      conn->key_hash == marten_hash_cstr("content-length")) {
    /* get content length value */
    marten_uint_parse(conn->test.length, flags, ptr, len);
  }
  
  return 0;
}

marten_define_handler(put_test_handler) {
  http_conn_t *conn = marten_containerof(state, http_conn_t, route);
  
  if (marten_is_raw(flags, marten_head)) {
    marten_head_parse(&conn->header, put_test_header, flags, ptr, len);
  }
  
  if (marten_is(flags, marten_body)) {
    /* skip body */
    if (conn->test.length > len) {
      conn->test.length -= len;
    } else {
      conn->test.length = 0;
      marten_respond(&conn->response, put_test_response);
      return 1;
    }
  }
  
  return 0;
}
