#include "http_conn.h"

#include "param.h"
#include "elastr.h"
#include "param-sio.h"
#include "param-estr.h"
#include PARAM_MODULES_H

#define numof(a) (sizeof(a) / sizeof(a[0]))

static const param_coll_t *const param_colls[] PARAM_ROM_ATTR = { PARAM_COLLS };

#if PARAM_ENUM || PARAM_CSTR || PARAM_HBIN
static inline uint8_t param_need_quotes(const param_desc_t *param) {
  switch (param_type(param)) {
#  if PARAM_ENUM
  case param_enum:
#  endif
#  if PARAM_CSTR
  case param_cstr:
#  endif
#  if PARAM_HBIN
  case param_hbin:
#  endif
#  if PARAM_IPV4
  case param_ipv4:
#  endif
#  if PARAM_MAC
  case param_mac:
#  endif
    return 1;
  default:
    return 0;
  }
}
#else
#define param_need_quotes(...) 0
#endif

#if PARAM_CONS
static void put_cons(const param_desc_t *param,
                     param_flag_t field,
                     const char *name,
                     size_t name_len,
                     estr_t *res) {
  const param_cell_t *cons = param_cons(param, field);
  
  if (cons == NULL)
    return;

#if HTTPD_USE_YAML
  estr_putsc(res, "\r\n  ");
  estr_putsn(res, name, name_len);
  estr_putsc(res, ": ");
  param_getestr_cons(param, field, res);
#else
  estr_putsc(res, ",\"");
  estr_putsn(res, name, name_len);
  estr_putsc(res, "\":");

  uint8_t need_quotes = param_need_quotes(param);
  
  if (need_quotes)
    estr_putc(res, '"');
  
  param_getestr_cons(param, field, res);

  if (need_quotes)
    estr_putc(res, '"');
#endif
}
#endif

#if PARAM_TEXT
static void put_text(const param_desc_t *param,
                     param_flag_t field,
                     const char *name,
                     size_t name_len,
                     estr_t *res) {
  const char *text = param_text(param, field);
  
  if (text == NULL)
    return;

#if HTTPD_USE_YAML
  estr_putsc(res, "\r\n  ");
  estr_putsn(res, name, name_len);
  estr_putsc(res, ": ");
  estr_putsr(res, text);
#else
  estr_putsc(res, ",\"");
  estr_putsn(res, name, name_len);
  estr_putsc(res, "\":\"");
  estr_putsq(res, text);
  estr_putc(res, '"');
#endif
}

#if PARAM_ENUM
static void put_enum_texts(const param_desc_t *param, estr_t *res) {
  param_size_t num = param_enum_count(param);
  if (num == 0) {
    return;
  }
#if HTTPD_USE_YAML
  estr_putsc(res, "\r\n  enum:");
  
  param_size_t idx = 0;
  for (; idx < num; idx++) {
    const char *name = param_enum_text(param, param_none, idx);
    if (name == NULL) continue;
    
    estr_putsc(res, "\r\n    ");
    estr_putsq(res, name);
    estr_putsc(res, ": ");
    
#if PARAM_INFO
    const char *text = param_enum_text(param, param_info, idx);
    if (text == NULL) {
#endif /* PARAM_INFO */
      estr_putsc(res, "null");
#if PARAM_INFO
    } else {
      estr_putsq(res, text);
    }
#endif /* PARAM_INFO */
  }
#else /* HTTPD_USE_YAML */
  estr_putsc(res, ",\"enum\":{\"");
  
  param_size_t idx = 0;
  for (; idx < num; idx++) {
    const char *name = param_enum_text(param, param_none, idx);
    if (name == NULL) continue;
    
    if (idx > 0) estr_putsc(res, "\",\"");
    estr_putsq(res, name);
    estr_putsc(res, "\":\"");
#if PARAM_INFO
    const char *text = param_enum_text(param, param_info, idx);
    if (text != NULL) {
      estr_putsq(res, text);
    }
#endif /* PARAM_INFO */
  }
  estr_putsc(res, "\"}");
#endif /* HTTPD_USE_YAML */
}
#else
#define put_enum_texts(param, ptr, end)
#endif

#endif

static void param_info_str(const param_desc_t *param, estr_t *res, marten_flags_t flag) {
  (void)flag;

#if HTTPD_USE_YAML
  estr_putsc(res, "- name: ");
  estr_putsq(res, param_name(param));
#else
  estr_putc(res, flag & marten_init ? '[' : ',');
  estr_putsc(res, "{\"name\":\"");
  estr_putsq(res, param_name(param));
#endif
  
#if HTTPD_USE_YAML
    estr_putsc(res, "\r\n  type: ");
#else
    estr_putsc(res, "\",\"type\":\"");
#endif
    
    param_getestr_type(param, res);
    
#if HTTPD_USE_YAML
    estr_putsc(res, "\r\n  size: ");
    estr_putud(res, param_size(param));
#else
    estr_putsc(res, "\",\"size\":");
    estr_putud(res, param_size(param));
#endif

#if HTTPD_USE_YAML
  estr_putsc(res, "\r\n  flag:\r\n");
#define ADD_FLAG(name)                            \
  if (param_flag(param) & param_##name)           \
    estr_putsc(res, "    - " #name "\r\n")
#else
  estr_putsc(res, ",\"flag\":[");
#define ADD_FLAG(name)                            \
  if (param_flag(param) & param_##name) {         \
    if (*estr_ptr(res, -1) != '[')                \
      estr_putc(res, ',');                        \
    estr_putsc(res, "\"" #name "\"");             \
  }
#endif
  
  ADD_FLAG(getable);
  ADD_FLAG(setable);
#if PARAM_PERS
  ADD_FLAG(persist);
#endif
#if PARAM_VIRT
  ADD_FLAG(virtual);
#endif

#if !HTTPD_USE_YAML
  estr_putc(res, ']');
#endif

#define ADD_CONS(name)                                        \
  put_cons(param, param_##name, #name, sizeof(#name)-1, res)
  
#if PARAM_DEF
  ADD_CONS(def);
#endif
  
#if PARAM_ENUM
  if (param_type(param) != param_enum) {
#endif
#if PARAM_MIN
    ADD_CONS(min);
#endif
#if PARAM_MAX
    ADD_CONS(max);
#endif
#if PARAM_STP
    ADD_CONS(stp);
#endif
#if PARAM_ENUM
  }
#endif

#if PARAM_TEXT
#define ADD_TEXT(name)                                        \
  put_text(param, param_##name, #name, sizeof(#name)-1, res)

#if PARAM_UNIT
  ADD_TEXT(unit);
#endif
#if PARAM_INFO
  ADD_TEXT(info);
#endif
#if PARAM_HINT
  ADD_TEXT(hint);
#endif
#if PARAM_ENUM
  if (param_type(param) == param_enum) {
    put_enum_texts(param, res);
  }
#endif
#endif/*PARAM_TEXT*/

#if !HTTPD_USE_YAML
  estr_putc(res, '}');
  if (flag & marten_done)
    estr_putc(res, ']');
#endif
  
  estr_putsc(res, "\r\n");
}

static marten_define_writer(get_param_response) {
  http_conn_t *conn = marten_containerof(writer, http_conn_t, response);
  
  marten_begin(writer);
  marten_status_cstr_write(writer, "HTTP/1.1", 200, "OK");
  marten_conn_header_write(writer, conn->state);
#if HTTPD_USE_YAML
  marten_header_cstr_write(writer, "Content-Type", "application/yaml");
#else
  marten_header_cstr_write(writer, "Content-Type", "application/json");
#endif
  marten_header_cstr_write(writer, "Transfer-Encoding", "chunked");
  marten_feed_write(writer);

  estr_heap_init(&conn->param.info.result, NULL);
  
  for (conn->param.info.param_coll = 0;
       conn->param.info.param_coll < numof(param_colls);
       conn->param.info.param_coll++) {
#define params PARAM_ROM_GET(param_colls[conn->param.info.param_coll])
    
    for (conn->param.info.param = 0;
         conn->param.info.param < PARAM_ROM_GET(params->count);
         conn->param.info.param++) {
      
      param_info_str(PARAM_ROM_GET(params->param) + conn->param.info.param,
                     &conn->param.info.result,
                     (conn->param.info.param_coll == 0 && conn->param.info.param == 0 ? marten_init : 0) |
                     (conn->param.info.param_coll + 1 == numof(param_colls) && conn->param.info.param + 1 == PARAM_ROM_GET(params->count) ? marten_done : 0));
      
      {
        estr_t str;
        estr_extern_init(&str, conn->buffer);
        
        estr_putux(&str, estr_len(&conn->param.info.result) - 2);
        estr_putsc(&str, "\r\n");
        
        marten_estr_write(writer, &str);
      }
      
      marten_estr_write(writer, &conn->param.info.result);
      
      estr_cut(&conn->param.info.result, 0);
    }
  }
  
  estr_release(&conn->param.info.result);
  
  marten_cstr_write(writer, "0\r\n\r\n");
  marten_end(writer);
}

marten_define_handler(get_param_handler) {
  (void)flags;
  (void)ptr;
  (void)len;
  
  http_conn_t *conn = marten_containerof(state, http_conn_t, route);
  
  marten_respond(&conn->response, get_param_response);
  
  return 1;
}

static marten_define_writer(put_param_error) {
  http_conn_t *conn = marten_containerof(writer, http_conn_t, response);
  
  marten_begin(writer);
  marten_status_cstr_write(writer, "HTTP/1.1", 400, "Bad Request");
  marten_conn_header_write(writer, conn->state);
#if HTTPD_USE_YAML
  marten_header_cstr_write(writer, "Content-Type", "application/yaml");
  marten_header_cstr_write(writer, "Content-Length", "22\r\n");
  marten_cstr_write(writer, "error: Invalid request");
#else
  marten_header_cstr_write(writer, "Content-Type", "application/json");
  marten_header_cstr_write(writer, "Content-Length", "27\r\n");
  marten_cstr_write(writer, "{\"error\":\"Invalid request\"}");
#endif
  
  marten_end(writer);
}

static marten_define_writer(put_param_response) {
  http_conn_t *conn = marten_containerof(writer, http_conn_t, response);
  marten_begin(writer);
  
  if (estr_len(&conn->param.data.result) > 0) {
    marten_status_cstr_write(writer, "HTTP/1.1", 200, "OK");
    marten_conn_header_write(writer, conn->state);
#if HTTPD_USE_YAML
    marten_header_cstr_write(writer, "Content-Type", "application/yaml");
#else
    marten_header_cstr_write(writer, "Content-Type", "application/json");
#endif
    
    estr_t str;
    estr_extern_init(&str, conn->buffer);
    
    estr_putsc(&str, "Content-Length: ");
    estr_putud(&str, estr_len(&conn->param.data.result));
    estr_putsc(&str, "\r\n\r\n");
    
    marten_estr_write(writer, &str);
    marten_estr_write(writer, &conn->param.data.result);
  } else {
    marten_status_cstr_write(writer, "HTTP/1.1", 204, "No Content");
    marten_conn_header_write(writer, conn->state);
    marten_header_cstr_write(writer, "Content-Length", "0\r\n");
  }
  
  marten_end(writer);
  estr_release(&conn->param.data.result);
}

static const param_desc_t *params_find(const estr_t *name) {
  const param_coll_t *const *params_ptr = param_colls,
    *const *params_end = param_colls + sizeof(param_colls) / sizeof(param_colls[0]);
  
  for (; params_ptr < params_end; ) {
    const param_desc_t *param = param_find(*params_ptr++, estr_str(name), estr_len(name));
    
    if (param != NULL) {
      return param;
    }
  }
  
  return NULL;
}

static inline void param_do_get(param_state_t *state) {
  state->data.param = params_find(&state->data.field);
  estr_cut(&state->data.field, 0);

#if HTTPD_USE_YAML
  estr_putsc(&state->data.result, "- ");
#else
  estr_putc(&state->data.result, estr_len(&state->data.result) > 0 ? ',' : '[');
#endif
  
  if (state->data.param != NULL && param_size(state->data.param) > 0) {
    uint8_t need_quotes = param_need_quotes(state->data.param);
    
    if (need_quotes)
      estr_putc(&state->data.result, '"');
    
    if (0 > param_getestr(state->data.param, &state->data.result)) {
      /* error */
      if (need_quotes) {
        estr_cut(&state->data.result, estr_len(&state->data.result) - 1);
      }
      goto put_null;
    } else {
      if (need_quotes)
        estr_putc(&state->data.result, '"');
    }
  } else {
    goto put_null;
  }

 end:
  state->data.param = NULL;
  
#if HTTPD_USE_YAML
  estr_putsc(&state->data.result, "\r\n");
#endif
  
#if !HTTPD_USE_YAML
  if (state->data.state == param_complete) {
    estr_putc(&state->data.result, ']');
  }
#endif
  return;

 put_null:
  estr_putsc(&state->data.result, "null");
  goto end;
}

static inline void param_do_set(param_state_t *state) {
  if (state->data.param != NULL) {
    const char *ptr = estr_str(&state->data.field);
    const char *end = ptr + estr_len(&state->data.field);
    
#if HTTPD_USE_YAML
    estr_putsr(&state->data.result, param_name(state->data.param));
#else
    estr_putsn(&state->data.result, estr_len(&state->data.result) > 0 ? ",\"" : "{\"", 2);
    estr_putsq(&state->data.result, param_name(state->data.param));
    estr_putsc(&state->data.result, "\":");
#endif
    
    if (param_size(state->data.param) > 0) {
      int res = param_setstr(state->data.param, &ptr, end);
      debug("setstr: %d", res);
      
      if (res == param_success) {
        estr_putsc(&state->data.result, "true");
      } else {
        estr_putsc(&state->data.result, "false");
      }
    } else {
      estr_putsc(&state->data.result, "null");
    }
    
#if HTTPD_USE_YAML
    estr_putsc(&state->data.result, "\r\n");
#endif
    
#if !HTTPD_USE_YAML
    if (state->data.state == param_complete) {
      estr_putc(&state->data.result, '}');
    }
#endif
    
    estr_cut(&state->data.field, 0);
    state->data.param = NULL;
  }
}

static int param_value_handler(json_tokenizer *t, json_tokenizer_flags flags, const char *ptr, size_t len) {
  param_state_t *state = marten_containerof(t, param_state_t, data.tokenizer);
  http_conn_t *conn = marten_containerof(state, http_conn_t, param);
  
  rawlog("param_value_handler: %d", ptr, len, flags);

  switch (state->data.state) {
    /* prepare xchange request */
  case param_prepare:
    switch (flags) {
    case json_object_open:
      state->data.state = param_set_key;
      goto final;
    case json_array_open:
      state->data.state = param_get_key;
      goto final;
    default:
      goto error;
    }
    /* get param receive key */
  case param_get_key:
    switch (flags) {
    case json_bool_chunk:
    case json_string_chunk:
    case json_number_chunk:
    case json_null_chunk:
      goto field;
    case json_array_close:
      state->data.state = param_complete;
    case json_el_separate:
      param_do_get(state);
      goto final;
    default:
      goto error;
    }

  case param_set_key:
    switch (flags) {
    case json_string_chunk:
      goto field;
    case json_kv_separate:
      state->data.state = param_set_val;
      state->data.param = params_find(&state->data.field);
      estr_cut(&state->data.field, 0);
      goto final;
    default:
      goto error;
    }

  case param_set_val:
    switch (flags) {
    case json_bool_chunk:
    case json_string_chunk:
    case json_number_chunk:
    case json_null_chunk:
      goto field;
    case json_el_separate:
      state->data.state = param_set_key;
      param_do_set(state);
      goto final;
    case json_object_close:
      state->data.state = param_complete;
      param_do_set(state);
      goto final;
    default:
      goto error;
    }

  default:
    goto error;
  }
  
 field:
  estr_putsn(&state->data.field, ptr, len);
  return 1;

 final:
  if (state->data.state == param_complete) {
    estr_release(&state->data.field);
    marten_respond(&conn->response, put_param_response);
  }
  return 1;

 error:
  return 0;
}

marten_define_handler(put_param_handler) {
  http_conn_t *conn = marten_containerof(state, http_conn_t, route);
  
  {
    param_state_t *pstate = &conn->param;
    
    if (marten_is(flags, marten_body)) {
      if (marten_has(flags, marten_init)) {
        json_tokenizer_init(&pstate->data.tokenizer);
        
        pstate->data.state = param_prepare;
        estr_heap_init(&pstate->data.field, NULL);
        estr_heap_init(&pstate->data.result, NULL);
      }
      
      if (json_tokenizer_success !=
          json_tokenizer_execute(&pstate->data.tokenizer,
                                 param_value_handler, ptr, len)) {
        estr_release(&pstate->data.field);
        estr_release(&pstate->data.result);
        
        marten_respond(&conn->response, put_param_error);
        return 1;
      }
      
      if (pstate->data.state == param_complete) {
        return 1;
      }
    }
    
    return 0;
  }
}
