#include "user_config.h"
#include "bios.h"
#include "sdk/add_func.h"
#include "hw/esp8266.h"
#include "user_interface.h"
#include "sdk/rom2ram.h"
#include "http_server.h"

#define log os_printf

void common_init(void);
void radio_init(void);

static void init_done_cb(void) {
  log("\n"
      "SDK Init - Ok\n"
      "Current 'heap' size: %u bytes\n"
      "TCP PCBs: %u\n",
      system_get_free_heap_size(),
      MEMP_NUM_TCP_PCB);
  log("Set CPU CLK: %u MHz\n",
      ets_get_cpu_frequency());

  common_init();
  radio_init();

  os_printf("Start http server\n");
  http_server_start();
}

void user_init(void) {
  if(eraminfo.size > 0)
    log("Found free IRAM: base: %p, size: %u bytes\n", eraminfo.base,  eraminfo.size);
  
  log("System memory:\n");
  system_print_meminfo();
  log("Start 'heap' size: %u bytes\n", system_get_free_heap_size());
  log("Set CPU CLK: %u MHz\n", ets_get_cpu_frequency());
  system_deep_sleep_set_option(0);

  MEMP_NUM_TCP_PCB = 10;
  system_init_done_cb(init_done_cb);

  gdbstub_init();
}
