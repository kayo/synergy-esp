#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "sdk/sdk_config.h"
#include "user_interface.h"
#include "sdk/add_func.h"

#define DEBUG_GROUP param,PARAM
#include "debug.h"

void radio_init(void);

#undef IP4_UINT
#define IP4_UINT(a, b, c, d) a, b, c, d

#include PARAMS_radio_params_H

static void radio_reconf(void);

enum {
  off = NULL_MODE,
  sta = STATION_MODE,
  ap = SOFTAP_MODE,
  stap = STATIONAP_MODE,
};

/* The op_mode's getter */
static int op_mode_get(const param_desc_t *param, param_cell_t *value) {
  (void)param;
  
  *(uint8_t*)value = op_mode;
  
  return param_success;
}

/* The op_mode's setter */
static int op_mode_set(const param_desc_t *param, const param_cell_t *value) {
  (void)param;

  op_mode = PARAM_ROM_GET(*(uint8_t*)value);
  radio_reconf();
  
  return param_success;
}

enum {
  b = PHY_MODE_11B,
  g = PHY_MODE_11G,
  n = PHY_MODE_11N,
};

/* The phy_mode's getter */
static int phy_mode_get(const param_desc_t *param, param_cell_t *value) {
  (void)param;
  
  *(uint8_t*)value = phy_mode;
  
  return param_success;
}

/* The phy_mode's setter */
static int phy_mode_set(const param_desc_t *param, const param_cell_t *value) {
  (void)param;
  
  phy_mode = PARAM_ROM_GET(*(uint8_t*)value);
  radio_reconf();
  
  return param_success;
}

/*
 * Access-Point Parameters
 */
static struct softap_config ap_config;

/* The ap ssid getter */
static int ap_ssid_get(const param_desc_t *param, param_cell_t *value) {
  (void)param;
  
  strcpy(value, ap_config.ssid);
  
  return param_success;
}

/* The ap ssid setter */
static int ap_ssid_set(const param_desc_t *param, const param_cell_t *value) {
  (void)param;
  
  PARAM_ROM_READ(ap_config.ssid, value, sizeof(ap_config.ssid));
  radio_reconf();
  
  return param_success;
}

/* The ap password getter */
static int ap_password_get(const param_desc_t *param, param_cell_t *value) {
  (void)param;
  
  strcpy(value, ap_config.password);
  
  return param_success;
}

/* The ap password setter */
static int ap_password_set(const param_desc_t *param, const param_cell_t *value) {
  (void)param;
  
  PARAM_ROM_READ(ap_config.password, value, sizeof(ap_config.password));
  radio_reconf();
  
  return param_success;
}

/* The ap_channel's getter */
static int ap_channel_get(const param_desc_t *param, param_cell_t *value) {
  (void)param;
  
  *(uint8_t*)value = ap_config.channel;
  
  return param_success;
}

/* The ap_channel's setter */
static int ap_channel_set(const param_desc_t *param, const param_cell_t *value) {
  (void)param;
  
  ap_config.channel = PARAM_ROM_GET(*(uint8_t*)value);
  radio_reconf();
  
  return param_success;
}

/* The ap_security getter */
static int ap_security_get(const param_desc_t *param, param_cell_t *value) {
  (void)param;
  
  *(uint8_t*)value = ap_config.authmode;
  
  return param_success;
}

/* The ap_security setter */
static int ap_security_set(const param_desc_t *param, const param_cell_t *value) {
  (void)param;
  
  ap_config.authmode = PARAM_ROM_GET(*(uint8_t*)value);
  radio_reconf();
  
  return param_success;
}

/* The ap_hidden's getter */
static int ap_hidden_get(const param_desc_t *param, param_cell_t *value) {
  (void)param;
  
  *(uint8_t*)value = ap_config.ssid_hidden;
  
  return param_success;
}

/* The ap_hidden's setter */
static int ap_hidden_set(const param_desc_t *param, const param_cell_t *value) {
  (void)param;
  
  ap_config.ssid_hidden = PARAM_ROM_GET(*(uint8_t*)value);
  radio_reconf();
  
  return param_success;
}

/* The ap_maxconn's getter */
static int ap_maxconn_get(const param_desc_t *param, param_cell_t *value) {
  (void)param;
  
  *(uint8_t*)value = ap_config.max_connection;
  
  return param_success;
}

/* The ap_maxconn's setter */
static int ap_maxconn_set(const param_desc_t *param, const param_cell_t *value) {
  (void)param;
  
  ap_config.max_connection = PARAM_ROM_GET(*(uint8_t*)value);
  radio_reconf();
  
  return param_success;
}

/* The ap_beacon's getter */
static int ap_beacon_get(const param_desc_t *param, param_cell_t *value) {
  (void)param;
  
  *(uint16_t*)value = ap_config.beacon_interval;
  
  return param_success;
}

/* The ap_beacon's setter */
static int ap_beacon_set(const param_desc_t *param, const param_cell_t *value) {
  (void)param;
  
  ap_config.beacon_interval = PARAM_ROM_GET(*(uint16_t*)value);
  radio_reconf();
  
  return param_success;
}

struct ip_info ap_iface;

/* The ap ip getter */
static int ap_ip_get(const param_desc_t *param, param_cell_t *value) {
  (void)param;
  
  *(struct ip_addr*)value = ap_iface.ip;
  
  return param_success;
}

/* The ap ip setter */
static int ap_ip_set(const param_desc_t *param, const param_cell_t *value) {
  (void)param;
  
  ap_iface.ip = PARAM_ROM_GET(*(struct ip_addr*)value);
  radio_reconf();
  
  return param_success;
}

/* The ap gateway getter */
static int ap_gateway_get(const param_desc_t *param, param_cell_t *value) {
  (void)param;
  
  *(struct ip_addr*)value = ap_iface.gw;
  
  return param_success;
}

/* The ap gateway setter */
static int ap_gateway_set(const param_desc_t *param, const param_cell_t *value) {
  (void)param;
  
  ap_iface.gw = PARAM_ROM_GET(*(struct ip_addr*)value);
  radio_reconf();
  
  return param_success;
}

/* The ap netmask getter */
static int ap_netmask_get(const param_desc_t *param, param_cell_t *value) {
  (void)param;
  
  *(struct ip_addr*)value = ap_iface.netmask;
  
  return param_success;
}

/* The ap netmask setter */
static int ap_netmask_set(const param_desc_t *param, const param_cell_t *value) {
  (void)param;
  
  ap_iface.netmask = PARAM_ROM_GET(*(struct ip_addr*)value);
  radio_reconf();
  
  return param_success;
}

/* The ap_dhcps's getter */
static int ap_dhcps_get(const param_desc_t *param, param_cell_t *value) {
  (void)param;
  
  *(uint8_t*)value = ap_dhcps;
  
  return param_success;
}

/* The ap_dhcps's setter */
static int ap_dhcps_set(const param_desc_t *param, const param_cell_t *value) {
  (void)param;
  
  ap_dhcps = PARAM_ROM_GET(*(uint8_t*)value);
  radio_reconf();
  
  return param_success;
}

struct dhcps_lease ap_dhcps_lease;

/* The ap dhcps_from getter */
static int ap_dhcps_from_get(const param_desc_t *param, param_cell_t *value) {
  (void)param;
  
  *(struct ip_addr*)value = ap_dhcps_lease.start_ip;
  
  return param_success;
}

/* The ap dhcps_from setter */
static int ap_dhcps_from_set(const param_desc_t *param, const param_cell_t *value) {
  (void)param;
  
  ap_dhcps_lease.start_ip = PARAM_ROM_GET(*(struct ip_addr*)value);
  radio_reconf();
  
  return param_success;
}

/* The ap dhcps to getter */
static int ap_dhcps_to_get(const param_desc_t *param, param_cell_t *value) {
  (void)param;
  
  *(struct ip_addr*)value = ap_dhcps_lease.end_ip;
  
  return param_success;
}

/* The ap dhcps to setter */
static int ap_dhcps_to_set(const param_desc_t *param, const param_cell_t *value) {
  (void)param;
  
  ap_dhcps_lease.end_ip = PARAM_ROM_GET(*(struct ip_addr*)value);
  radio_reconf();
  
  return param_success;
}

/* The ap_stations's getter */
static int ap_stations_get(const param_desc_t *param, param_cell_t *value) {
  (void)param;
  
  *(uint8_t*)value = wifi_softap_get_station_num();
  
  return param_success;
}

static struct station_config st_config;

/*
#define int2hex(i) ((i) <= 9 ? (i) + '0' : (i) - 10 + 'a')
#define hex2int(c) ((c) <= '9' ? (c) - '0' : ((c) <= 'F' ? (c) - 'A' : (c) - 'a') + 10)
*/

static inline char int2hex(uint8_t i) {
  return i <= 9 ? i + '0' : i - 10 + 'a';
}

static inline uint8_t hex2int(char c) {
  return c <= '9' ? c - '0' : (c <= 'F' ? c - 'A' : c - 'a') + 10;
}

static inline int8_t is_hex(char c) {
  return (c >= '0' && c <= '9') || (c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f');
}

/* The st ssid getter */
static int st_ssid_get(const param_desc_t *param, param_cell_t *value) {
  (void)param;
  
  if (st_config.bssid_set) {
    char *ptr = value;
    uint8_t i = 0;
    
    for (;
         i < sizeof(st_config.bssid) / sizeof(st_config.bssid[0]);
         i++) {
      *ptr++ = int2hex(st_config.bssid[i] >> 4);
      *ptr++ = int2hex(st_config.bssid[i] & 0xf);
      *ptr++ = ':';
    }
    *ptr = '\0';
  } else {
    strcpy(value, st_config.ssid);
  }
  
  return param_success;
}

/* The st ssid setter */
static int st_ssid_set(const param_desc_t *param, const param_cell_t *value) {
  (void)param;

  char ssid[sizeof(st_config.ssid)];
  PARAM_ROM_READ(ssid, value, sizeof(ssid));
  
  const char *ptr = ssid;
  uint8_t i = 0, h = 0;
  
  /* check bssid */
  for (; *ptr != '\0'; ptr++) {
    if (is_hex(*ptr)) {
      if (h < 2) {
        h++;
      } else {
        break;
      }
    } else if (*ptr == ':') {
      i++;
      h = 0;
    } else {
      break;
    }
  }
  ptr = value;
  
  if (i == sizeof(st_config.bssid) / sizeof(st_config.bssid[0])) {
    st_config.bssid_set = 1;
    /* set bssid */
    for (i = 0;
         i < sizeof(st_config.bssid) / sizeof(st_config.bssid[0]);
         i++, ptr++) {
      st_config.bssid[i] = hex2int(*ptr++) << 4;
      st_config.bssid[i] |= hex2int(*ptr++);
    }
    debug("set bssid: %02x:%02x:%02x:%02x:%02x:%02x\n",
          st_config.bssid[0], st_config.bssid[1], st_config.bssid[2],
          st_config.bssid[3], st_config.bssid[4], st_config.bssid[5]);
  } else {
    st_config.bssid_set = 0;
    /* set ssid */
    strncpy(st_config.ssid, ptr, sizeof(st_config.ssid)-1);
    debug("set ssid: %s\n", st_config.ssid);
  }
  radio_reconf();
  
  return param_success;
}

/* The st ssid getter */
static int st_password_get(const param_desc_t *param, param_cell_t *value) {
  (void)param;
  
  strcpy(value, st_config.password);
  
  return param_success;
}

/* The st ssid setter */
static int st_password_set(const param_desc_t *param, const param_cell_t *value) {
  (void)param;
  
  PARAM_ROM_READ(st_config.password, value, sizeof(st_config.password));
  
  debug("set password: %s\n", st_config.password);
  radio_reconf();
  
  return param_success;
}

/* The st_dhcpc's getter */
static int st_dhcpc_get(const param_desc_t *param, param_cell_t *value) {
  (void)param;
  
  *(uint8_t*)value = st_dhcpc;
  
  return param_success;
}

/* The st_dhcpc's setter */
static int st_dhcpc_set(const param_desc_t *param, const param_cell_t *value) {
  (void)param;
  
  st_dhcpc = PARAM_ROM_GET(*(uint8_t*)value);
  radio_reconf();
  
  return param_success;
}

struct ip_info st_iface;

/* The st ip getter */
static int st_ip_get(const param_desc_t *param, param_cell_t *value) {
  (void)param;
  
  *(struct ip_addr*)value = st_iface.ip;
  
  return param_success;
}

/* The st ip setter */
static int st_ip_set(const param_desc_t *param, const param_cell_t *value) {
  (void)param;
  
  st_iface.ip = PARAM_ROM_GET(*(struct ip_addr*)value);
  radio_reconf();
  
  return param_success;
}

/* The st gateway getter */
static int st_gateway_get(const param_desc_t *param, param_cell_t *value) {
  (void)param;
  
  *(struct ip_addr*)value = st_iface.gw;
  
  return param_success;
}

/* The st gateway setter */
static int st_gateway_set(const param_desc_t *param, const param_cell_t *value) {
  (void)param;
  
  st_iface.gw = PARAM_ROM_GET(*(struct ip_addr*)value);
  radio_reconf();
  
  return param_success;
}

/* The st netmask getter */
static int st_netmask_get(const param_desc_t *param, param_cell_t *value) {
  (void)param;
  
  *(struct ip_addr*)value = st_iface.netmask;
  
  return param_success;
}

/* The st netmask setter */
static int st_netmask_set(const param_desc_t *param, const param_cell_t *value) {
  (void)param;
  
  st_iface.netmask = PARAM_ROM_GET(*(struct ip_addr*)value);
  radio_reconf();
  
  return param_success;
}

/* The st_rssi's getter */
static int st_rssi_get(const param_desc_t *param, param_cell_t *value) {
  (void)param;
  
  *(int8_t*)value = wifi_station_get_rssi();
  
  return param_success;
}

static char st_hostname[32];

/* The st_hostname's getter */
static int st_hostname_get(const param_desc_t *param, param_cell_t *value) {
  (void)param;
  
  debug("get st hostname: %s", st_hostname);
  strcpy(value, st_hostname);
  
  return param_success;
}

/* The st_hostname's setter */
static int st_hostname_set(const param_desc_t *param, const param_cell_t *value) {
  (void)param;

  PARAM_ROM_READ(st_hostname, value, sizeof(st_hostname));
  debug("set st hostname: %s", st_hostname);
  radio_reconf();
  
  return param_success;
}

#define __radio_params_c__
#include PARAMS_radio_params_H

static os_timer_t conf_timer;

static void conf_apply(void *arg) {
  (void)arg;
  
  if (wifi_get_opmode() & STATION_MODE /*&& !(op_mode & STATION_MODE)*/) {
    debug("st disconnect \n");
    wifi_station_disconnect();
  }
  
  if (op_mode != wifi_get_opmode()) {
    debug("op mode: %u\n", op_mode);
    wifi_set_opmode_current(op_mode);
  }

  if (phy_mode != wifi_get_phy_mode()) {
    debug("phy mode: %u\n", phy_mode);
    /* SoftAP mode doesn't support 802.11N */
    if (op_mode & SOFTAP_MODE &&
        phy_mode == PHY_MODE_11N) {
      wifi_set_phy_mode(PHY_MODE_11G);
    } else {
      wifi_set_phy_mode(phy_mode);
    }
  }
  
  if (op_mode & SOFTAP_MODE) {
    debug("ap config: ssid: %s, password: %s, hidden: %s\n",
          ap_config.ssid, ap_config.password, ap_config.ssid_hidden ? "yes" : "no");
    wifi_softap_set_config_current(&ap_config);
    
    if (wifi_softap_dhcps_status() == DHCP_STARTED) {
      wifi_softap_dhcps_stop();
    }
    
    wifi_set_ip_info(SOFTAP_IF, &ap_iface);
    
    if (ap_dhcps) {
      wifi_softap_set_dhcps_lease(&ap_dhcps_lease);
      wifi_softap_dhcps_start();
    }
  }
  
  if (op_mode & STATION_MODE) {
    debug("st config: ssid: %s, password: %s, bssid_set: %s\n",
          st_config.ssid, st_config.password, st_config.bssid_set ? "yes" : "no");
    wifi_station_set_config_current(&st_config);

    if (wifi_station_dhcpc_status() == DHCP_STARTED) {
      wifi_station_dhcpc_stop();
    }
    
    if (st_dhcpc) {
      wifi_station_dhcpc_start();
    } else {
      wifi_set_ip_info(STATION_IF, &st_iface);
    }
    
    wifi_station_connect();
  }
}

static void radio_reconf(void) {
  os_timer_disarm(&conf_timer);
  os_timer_arm(&conf_timer, 1000, 0);
}

static void radio_handle_event(System_Event_t *evt) {
  os_printf("WiFi event %x\n", evt->event);
  switch (evt->event) {
  case EVENT_STAMODE_CONNECTED:
    debug("Connect to ssid %s, channel %d\n",
          evt->event_info.connected.ssid,
          evt->event_info.connected.channel);
    break;
  case EVENT_STAMODE_DISCONNECTED:
    debug("Disconnect from ssid %s, reason %d\n",
          evt->event_info.disconnected.ssid,
          evt->event_info.disconnected.reason);
    break;
  case EVENT_STAMODE_AUTHMODE_CHANGE:
    debug("New AuthMode: %d -> %d\n",
          evt->event_info.auth_change.old_mode,
          evt->event_info.auth_change.new_mode);
    break;
  case EVENT_STAMODE_GOT_IP:
    debug("Station ip:" IPSTR ", mask:" IPSTR ", gw:" IPSTR "\n",
          IP2STR(&evt->event_info.got_ip.ip),
          IP2STR(&evt->event_info.got_ip.mask),
          IP2STR(&evt->event_info.got_ip.gw));
    break;
  case EVENT_SOFTAPMODE_STACONNECTED:
    debug("Station[%u]: " MACSTR " join, AID = %d\n",
          wifi_softap_get_station_num(),
          MAC2STR(evt->event_info.sta_connected.mac),
          evt->event_info.sta_connected.aid);
    break;
  case EVENT_SOFTAPMODE_STADISCONNECTED:
    debug("Station[%u]: " MACSTR " leave, AID = %d\n",
          wifi_softap_get_station_num(),
          MAC2STR(evt->event_info.sta_disconnected.mac),
          evt->event_info.sta_disconnected.aid);
    break;
  }
}

extern uint8_t *hostname;
extern bool default_hostname;

void radio_init(void) {
  default_hostname = false;
  hostname = st_hostname;
  
  /* setup reconf timer */
  os_timer_setfn(&conf_timer, conf_apply, NULL);
  
  /* init radio params */
  param_init(&radio_params);
  
  /* set wifi events handler */
  wifi_set_event_handler_cb(radio_handle_event);
}
