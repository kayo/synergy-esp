#include <stddef.h>
#include <stdint.h>

#include "sdk/sdk_config.h"
#include "user_interface.h"
#include "sdk/add_func.h"

#define DEBUG_GROUP param,PARAM
#include "debug.h"

void common_init(void);

#include PARAMS_common_params_H

extern char _heap_start;
#define _heap_end (*(char*)0x3fffc000)
#define _heap_size ((uint32_t)(&_heap_end - &_heap_start))
#define free_heap_init() free_heap_cons[1] = _heap_size;

/* The free heap getter */
static int free_heap_get(const param_desc_t *param, param_cell_t *value) {
  (void)param;
  
  *(uint32_t*)value = system_get_free_heap_size();
  debug("free_heap_get(): %u", *(uint32_t*)value);
  
  return param_success;
}

#define __common_params_c__
#include PARAMS_common_params_H

void common_init(void) {
  /* setup sys params */
  //free_heap_init();

  /* init common params */
  param_init(&common_params);
}
