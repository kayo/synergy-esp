#include "http_conn.h"

static marten_define_writer(get_pong_response) {
  http_conn_t *conn = marten_containerof(writer, http_conn_t, response);
  marten_begin(writer);
  marten_status_cstr_write(writer, "HTTP/1.1", 200, "OK");
  marten_conn_header_write(writer, conn->state);
  marten_header_cstr_write(writer, "Content-Type", "text/plain");
  marten_header_cstr_write(writer, "Content-Length", "4");
  marten_feed_write(writer);
  marten_cstr_write(writer, "pong");
  marten_end(writer);
}

marten_define_handler(get_ping_handler) {
  (void)flags;
  (void)ptr;
  (void)len;
  
  http_conn_t *conn = marten_containerof(state, http_conn_t, route);
  
  marten_respond(&conn->response, get_pong_response);
  
  return 1;
}
